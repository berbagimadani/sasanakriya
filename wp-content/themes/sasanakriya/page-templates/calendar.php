<?php
/**
 * Template Name: Calendar Page
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

	
	<?php while ( have_posts() ) : the_post(); ?>
				
		<?php the_content() ?>

	<?php endwhile; ?>

	<!-- 
	<div class="uk-grid uk-grid-small margin-content padding-page">
		<div class="uk-width-large-1-4 uk-width-small-1 uk-width-medium-3-10 uk-text-center-small uk-margin-top">
    		<h4>Informasi <dd style="padding-left:20px">Hubungi</dd> </h4>

    	</div>
		<div class="uk-width-large-1-2 uk-width-small-1 uk-width-medium-7-10 uk-text-center">
			<h1> <?php the_title() ?> </h1>
    	</div>
    	<div class="uk-width-large-1-4 uk-width-small-1 uk-width-medium-7-10 uk-text-left">
			&nbsp;
    	</div>
	</div>


	<div class="uk-grid uk-grid-small margin-content padding-page grid-calendar">
	
	<div class="uk-width-large-3-10">
	content
	</div>-->

	<!-- CALENDAR -->
        <div class="uk-width-large-7-10 uk-calendar-right">
            <div class="uk-grid uk-grid-small uk-calendar-right">  
                <div class="uk-width-large-1-1">
                    
                     
                    <div class="uk-grid">
                            <div class="uk-width-large-1-3 uk-width-medium-1-3 uk-width-small-1-3">
                                <div class="uk-grid uk-grid-collapse">
                                    <div class="uk-width-large-1-5 uk-width-medium-1-5 uk-width-small-1-5 uk-text-left"><a href="#" class="cal-arrow-left"></a></div>
                                    <div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1-2 uk-text-center uk-cal-ym"><span class="cal-ym"></span></div>
                                    <div class="uk-width-large-1-5 uk-width-medium-1-5 uk-width-small-1-5 uk-text-right"><a href="#" class="cal-arrow-right"></a></div>
                                </div>
                            </div>
                            <div class="uk-width-large-1-3 uk-width-medium-1-3 uk-width-small-1-3">
                                &nbsp;
                            </div>
                            <div class="uk-width-large-1-3 uk-width-medium-1-3 uk-width-small-1-3 uk-text-right">
                                <div class="uk-grid uk-grid-collapse">
                                    <div class="uk-width-large-1-5 uk-width-medium-1-5 uk-width-small-1-5 uk-text-left"><a href="#" class="cal-arrow-left"></a></div>
                                    <div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1-2 uk-text-center uk-cal-ym"><span class="cal-ym"></span></div>
                                    <div class="uk-width-large-1-5 uk-width-medium-1-5 uk-width-small-1-5 uk-text-right"><a href="#" class="cal-arrow-right"></a></div>
                                </div>
                            </div>
                    </div>
                    

                
                    <div class="uk-grid uk-grid-small margin-calendar">
                        <table >
                          <tr class="daily-head">
                            <td>Minggu</td>
                            <td>Senin</td>
                            <td>Selasa</td>
                            <td>Rabu</td>
                            <td>Kamis</td>
                            <td>Jum'at</td>
                            <td>Sabtu</td>
                          </tr>
                          <tr class="daily">
                            <td class="daily-bg"></td>
                            <td class="daily-bg"></td>
                            <td class="daily-bg"> 
                                <div class="daily-box">
                                    <!--
                                    <div class="daily-box-one"><span class="daily-date">1</span></div>
                                    <div class="daily-box-two" ><span class="m-confirm-block"><div class="m-tentative night-1"></div></span></div>
                                    <div class="daily-box-tree">
                                        <span class="m-confirm-block"><div class="m-confirm night-top"></div></span>
                                        <span class="m-confirm-block"><div class="c-confirm night-bottom"></div></span>
                                    </div>
                                    <div class="daily-box-four"><span class="m-confirm-block"><div class="c-tentative night-2"></div></span></div>
                                    -->
                                </div>
                            </td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                          </tr>
                          <tr class="daily">
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                          </tr>
                          <tr class="daily">
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                          </tr>
                          <tr class="daily"> 
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                          </tr>
                          <tr class="daily">
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"></td>
                            <td class="daily-bg"></td>
                          </tr>
                          <tr class="daily">
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"></td>
                            <td class="daily-bg"></td>
                          </tr>
                        </table> 
                    </div>
                </div>
            </div> 
        </div>
        <!-- // CALENDAR -->
	
	</div>
	
	<!-- LINE -->
	<div class="uk-grid margin-content">
		<div class="uk-width-large-1">
    		<hr class="line">
    	</div>
	</div>

<?php get_footer(); ?>