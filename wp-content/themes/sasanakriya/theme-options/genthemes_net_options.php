<?php
define('GENTHEMES_SHORTNAME', 'genthemesfr');  
define('GENTHEMES_PAGE_BASENAME', 'genthemesfr-settings');
/*
 * Specify Hooks/Filters
 */
add_action( 'admin_menu', 'genthemesfr_add_menu' );  
add_action( 'admin_init', 'genthemesfr_register_settings' );
require_once ( get_stylesheet_directory() . '/theme-options/class.functions.php');

 /**
 * Helper function for defining variables for the current page
 *
 * @return array
 */
if ( is_admin() ) :
function genthemesfr_get_settings() { 
	$output = array(); 
	// put together the output array
	$output['genthemesfr_option_name']			= 'genthemesfr_options';  
	$output['genthemesfr_option_theme_name'] 	= 'genthemesfr_options_theme'; 
	$output['genthemesfr_page_title']			= 'GenThemes Framework Settings Page'; 
	return $output;
}

/*
 * Register our setting
 */
function genthemesfr_register_settings(){  
	$settings_output = genthemesfr_get_settings();
	$genthemesfr_option_name = $settings_output['genthemesfr_option_name'];
	$genthemesfr_option_theme_name = $settings_output['genthemesfr_option_theme_name'];
	
	//register_setting( $option_group, $option_name, $sanitize_callback );
	register_setting($genthemesfr_option_name, $genthemesfr_option_name, 'genthemesfr_validate_options' );
	 
}
function genthemesfr_net_settings_scripts(){
	$settings_output = genthemesfr_get_settings();
	$genthemesfr_option_name = $settings_output['genthemesfr_option_name'];
	 
	if ( 'appearance_page_genthemesfr-settings' == get_current_screen() -> id ) {
		wp_enqueue_script('jquery');
		wp_enqueue_script('media-upload');
		wp_enqueue_style('thickbox');
		wp_enqueue_script('thickbox');
		wp_register_script('my-upload', get_template_directory_uri().'/theme-options/assets/js/media_upload.js', array('jquery','media-upload','thickbox'));
		wp_enqueue_script('my-upload'); 
		
		wp_register_script('gen-tab', get_template_directory_uri().'/theme-options/assets/js/tab.js', array('jquery'), false, true);
		wp_enqueue_script('gen-tab'); 
		
     	wp_enqueue_style('genthemesfr_net_styles_css', get_template_directory_uri() .'/theme-options/assets/css/styles.css');
    	wp_enqueue_style('genthemesfr_net_opensans_css', 'http://fonts.googleapis.com/css?family=Open+Sans:400,700');    
		
    	wp_enqueue_script( 'genthemesv1-theme-options', get_template_directory_uri() . '/theme-options/theme-options.js', array( 'farbtastic' ), '2011-06-10' );
		wp_enqueue_style( 'farbtastic' );
	}
}
add_action('admin_print_styles-appearance_page_genthemesfr-settings', 'genthemesfr_net_settings_scripts');

function genthemesfr_add_menu(){ 
	$genthemesfr_settings_page = add_theme_page('Genthemes Options','GenthemesOptions','manage_options', GENTHEMES_PAGE_BASENAME, 'genthemesfr_settings_page_fn');	 
}
function genthemesfr_settings_page_fn() {
	$m = new GtFormDisplay();
	$m->display();
}
function genthemesfr_show_msg($message, $msgclass = 'info'){
	echo "<div id='message' class='$msgclass'>$message</div>";
}
function genthemesfr_validate_options( $input ) {
	
	if ( isset( $input['favicon'] ) ) {
		$valid['favicon'] = wp_filter_post_kses( $input['favicon'] );
	}
	if ( isset( $input['logo'] ) ) {
		$valid['logo'] = wp_filter_post_kses( $input['logo'] );
	}
	if ( isset( $input['description'] ) ) {
		$valid['description'] = wp_filter_post_kses( $input['description'] );
	}
	if ( isset( $input['keyword'] ) ) {
		$valid['keyword'] = wp_filter_post_kses( $input['keyword'] );
	}
	if ( isset( $input['bgradio'] ) ) {
		$valid['bgradio'] = wp_filter_post_kses( $input['bgradio'] );
	}
	if ( isset( $input['attr_bgimage'] ) ) {
		$valid['attr_bgimage'] = wp_filter_post_kses( $input['attr_bgimage'] );
	}
	if ( isset( $input['bgimage'] ) ) {
		$valid['bgimage'] = wp_filter_post_kses( $input['bgimage'] );
	}
	if ( isset( $input['bgpattern'] ) ) {
		$valid['bgpattern'] = wp_filter_post_kses( $input['bgpattern'] );
	}
	if ( isset( $input['bgcolor'] ) ) {
		$valid['bgcolor'] = wp_filter_post_kses( $input['bgcolor'] );
	}
	if ( isset( $input['gt_social_show'] ) ) {
		$valid['gt_social_show'] = wp_filter_post_kses( $input['gt_social_show'] );
	}
	if ( isset( $input['gt_social_check'] ) ) {
		$valid['gt_social_check'] = wp_filter_post_kses( $input['gt_social_check'] );
	}	
	
	
	return apply_filters( 'genthemesfr_validate_options', $input );   
}
function unique_identifyer_admin_notices() {
	if(isset($_GET['page'])){
	$enthemesfr_settings_pg = strpos($_GET['page'], GENTHEMES_PAGE_BASENAME);
	$set_errors = get_settings_errors(); 
	if(current_user_can ('manage_options') && $enthemesfr_settings_pg !== FALSE && !empty($set_errors)){ 
		if ($set_errors[0]['code'] == 'settings_updated' && isset( $_GET['settings-updated']) ){
			genthemesfr_show_msg("<p>" . $set_errors[0]['message'] . "</p>", 'updated');
		}else{
			// there maybe more than one so run a foreach loop.
			foreach($set_errors as $set_error){
				// set the title attribute to match the error "setting title" - need this in js file
				wptuts_show_msg("<p class='setting-error-message' title='" . $set_error['setting'] . "'>" . $set_error['message'] . "</p>", 'error');
			}
		}
	}}
}
add_action( 'admin_notices', 'unique_identifyer_admin_notices' );
endif; 