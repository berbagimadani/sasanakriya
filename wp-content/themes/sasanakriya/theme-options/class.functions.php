<?php 
		
class GtFormDisplay{
	
	function display(){
		$root = get_template_directory_uri();
		$folder = './images/';
		$settings_output = genthemesfr_get_settings();
		$genthemesfr_option_name = $settings_output['genthemesfr_option_name'];
		if ( ! isset( $_REQUEST['updated'] ) )
			$_REQUEST['updated'] = false; // This checks whether the form has just been submitted.
			 
		?>
	  
		<div class="row-gen left wh-gen">  
	       <div class="panel callout-custome radius">
	    	   <h5>GENTHEMES OPTIONS PANEL </h5> 
	       </div> 
	        
	       	<dl class="tabs-genthemes vertical bg-tab" data-tab="">
              <dd><a href="#header"><span>General</span></a></dd> 
            </dl>
	         
	      
	        	<div class="tabs-genthemes-content vertical">  
		            <?php $options = get_option($genthemesfr_option_name); ?> 
		            
		            <form method="post" action="options.php">
		           	<?php settings_fields( $genthemesfr_option_name );?>
		           	
		           	<div class="content-tab" id="header"> <!-- header -->
		                 
		                   <a href="#" class="label tiny radius 20 success pad-bottom">Header</a>
		                   
		                   <label>Favicon</label>  
		                    <input type="text" class="<?php echo $genthemesfr_option_name;?>_favicon" id="<?php echo $genthemesfr_option_name;?>_favicon" name="<?php echo $genthemesfr_option_name;?>[favicon]" value="<?php if(isset($options['favicon'])) { echo $options['favicon']; }?>">
		 			 		<input type="button" id="select-<?php echo $genthemesfr_option_name;?>_favicon" class="upload_image_button button" value="Select Image" name="<?php echo $genthemesfr_option_name;?>_favicon">
		 			 		<?php if(!empty($options['favicon'])) { ?>
		 			 		<input type="button" id="select-<?php echo $genthemesfr_option_name;?>_favicon" class="remove_image_button button" value="Remove Image" name="<?php echo $genthemesfr_option_name;?>_favicon">
		 			 		<?php }?>
							<div class="<?php echo $genthemesfr_option_name;?>_favicon" style="padding-top:10px"> 
		                      <?php if(isset($options['favicon'])) { echo "<img src='".$options['favicon']."' style='width:20%'>"; }?>
		                   	</div> 
		 			 			 
		                  <hr>  

		                  <label>Fb footer</label>  
		                    <input type="text" class="<?php echo $genthemesfr_option_name;?>_fb" id="<?php echo $genthemesfr_option_name;?>_fb" name="<?php echo $genthemesfr_option_name;?>[fb]" value="<?php if(isset($options['fb'])) { echo $options['fb']; }?>">
		 			 		<input type="button" id="select-<?php echo $genthemesfr_option_name;?>_fb" class="upload_image_button button" value="Select Image" name="<?php echo $genthemesfr_option_name;?>_fb">
		 			 		<?php if(!empty($options['favicon'])) { ?>
		 			 		<input type="button" id="select-<?php echo $genthemesfr_option_name;?>_fb" class="remove_image_button button" value="Remove Image" name="<?php echo $genthemesfr_option_name;?>_fb">
		 			 		<?php }?>
							<div class="<?php echo $genthemesfr_option_name;?>_fb" style="padding-top:10px"> 
		                      <?php if(isset($options['fb'])) { echo "<img src='".$options['fb']."' style='width:20%'>"; }?>
		                   	</div> 
		 			 		
		 			 		 <label>#Link Fb</label>
		                   <input type="text" placeholder="Enter your link_fb" name="<?php echo $genthemesfr_option_name;?>[link_fb]" value="<?php if(isset($options['link_fb'])) { echo esc_attr($options['link_fb']); }?>"/>	 
		                  <hr>  

		                  <label>TMII footer</label>  
		                    <input type="text" class="<?php echo $genthemesfr_option_name;?>_tmii" id="<?php echo $genthemesfr_option_name;?>_tmii" name="<?php echo $genthemesfr_option_name;?>[tmii]" value="<?php if(isset($options['tmii'])) { echo $options['tmii']; }?>">
		 			 		<input type="button" id="select-<?php echo $genthemesfr_option_name;?>_tmii" class="upload_image_button button" value="Select Image" name="<?php echo $genthemesfr_option_name;?>_tmii">
		 			 		<?php if(!empty($options['tmii'])) { ?>
		 			 		<input type="button" id="select-<?php echo $genthemesfr_option_name;?>_tmii" class="remove_image_button button" value="Remove Image" name="<?php echo $genthemesfr_option_name;?>_tmii">
		 			 		<?php }?>
							<div class="<?php echo $genthemesfr_option_name;?>_tmii" style="padding-top:10px"> 
		                      <?php if(isset($options['tmii'])) { echo "<img src='".$options['tmii']."' style='width:20%'>"; }?>
		                   	</div> 
		 			 		
		 			 		<label>#TMII LINk</label>
		                   <input type="text" placeholder="Enter your link tmii" name="<?php echo $genthemesfr_option_name;?>[link_tmii]" value="<?php if(isset($options['link_tmii'])) { echo esc_attr($options['link_tmii']); }?>"/>	 
		                  <hr>  

		                  <label>Weeding footer</label>  
		                    <input type="text" class="<?php echo $genthemesfr_option_name;?>_weeding" id="<?php echo $genthemesfr_option_name;?>_weeding" name="<?php echo $genthemesfr_option_name;?>[weeding]" value="<?php if(isset($options['weeding'])) { echo $options['weeding']; }?>">
		 			 		<input type="button" id="select-<?php echo $genthemesfr_option_name;?>_weeding" class="upload_image_button button" value="Select Image" name="<?php echo $genthemesfr_option_name;?>_weeding">
		 			 		<?php if(!empty($options['weeding'])) { ?>
		 			 		<input type="button" id="select-<?php echo $genthemesfr_option_name;?>_weeding" class="remove_image_button button" value="Remove Image" name="<?php echo $genthemesfr_option_name;?>_weeding">
		 			 		<?php }?>
							<div class="<?php echo $genthemesfr_option_name;?>_weeding" style="padding-top:10px"> 
		                      <?php if(isset($options['weeding'])) { echo "<img src='".$options['weeding']."' style='width:20%'>"; }?>
		                   	</div> 
		 			 		
		 			 		<label>#Weeding LINk</label>
		                   <input type="text" placeholder="Enter your link weeding" name="<?php echo $genthemesfr_option_name;?>[link_weeding]" value="<?php if(isset($options['link_weeding'])) { echo esc_attr($options['link_weeding']); }?>"/>	 
		                  <hr>  

		                   <label>@Copyright</label>
		                   <input type="text" placeholder="Enter your copyright" name="<?php echo $genthemesfr_option_name;?>[copyright]" value="<?php if(isset($options['copyright'])) { echo esc_attr($options['copyright']); }?>"/>

		                   <label>Design By</label>
		                   <input type="text" placeholder="Enter your deisgn by" name="<?php echo $genthemesfr_option_name;?>[design_by]" value="<?php if(isset($options['design_by'])) { echo esc_attr($options['design_by']); }?>"/>
		                   
		                   <label>Descriptions</label>
		                   <input type="text" placeholder="Enter your meta description" name="<?php echo $genthemesfr_option_name;?>[description]" value="<?php if(isset($options['description'])) { echo esc_attr($options['description']); }?>"/>
		                 
		                   <label>Keywords</label>
		                   <input type="text" placeholder="Enter your meta keyword" name="<?php echo $genthemesfr_option_name;?>[keyword]" value="<?php if(isset($options['keyword'])) { echo esc_attr($options['keyword']); }?>"/>
		                
		                    
		                   <label>Google Analytic Code</label>
		                   <textarea placeholder="Google analytic code" class="custome-textarea" name="<?php echo $genthemesfr_option_name;?>[google-analytic]"><?php if(isset($options['google-analytic'])) { echo esc_attr($options['google-analytic']); }?></textarea>
		                 
					</div>  <!-- // header -->
					 
		                  
		            	 
				        <label>
			              <!-- BUTTON -->  
			              <input type="submit" class="button-primary" value="Save Options"> <span id="progress"></span>
			              <!-- //BUTTON -->
			            </label>
	          		</form>   
	      		</div>
	      		
	    </div>
<?php
	}
}

?>