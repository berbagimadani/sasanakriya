<?php

function caption_shortcode( $atts, $content = null ) {

 	extract( shortcode_atts( array(
		'title' => '', 
	), $atts ) );

 	$html = '
 		<div class="uk-width-large-1-2 phone-daftar-description">     
    		<h2>'.$title.'</h2>
    		<ul class="font-medium">
    		'.do_shortcode($content).'
    		</ul>
    	</div>';

	return $html;
}
add_shortcode( 'one', 'caption_shortcode' );

function li_sort( $atts, $content = null ){
	$c = do_shortcode( $content );
	return '<li>'.$c.'</li>';
}
add_shortcode( 'li', 'li_sort' );

/**
*	Bread Crumb
*/
function breadcrumb_shortcode( $atts, $content = null ) {

 	extract( shortcode_atts( array(
		'title' => '', 
	), $atts ) );

 	$html = '
 		<div class="uk-grid uk-grid-small margin-content padding-page">
		<div class="uk-width-large-1-4 uk-width-small-1 uk-width-medium-3-10 uk-text-center-small">
    		 
            '.do_shortcode( $content ).'

    	</div>
		<div class="uk-width-large-1-2 uk-width-small-1 uk-width-medium-7-10 uk-text-center">
			<h1>'.$title.'</h1>
    	</div>
    	<div class="uk-width-large-1-4 uk-width-small-1 uk-width-medium-7-10 uk-text-left">
			&nbsp;
    	</div>
	</div>';

	return $html;
}
add_shortcode( 'breadcrumb', 'breadcrumb_shortcode' );

function level_first( $atts, $content = null ){
	extract( shortcode_atts( array(
		'link' => '', 
	), $atts ) );

	$c = do_shortcode( $content );
	return '<dd class="level1"><a href="'.get_permalink().'">'.$c.'</a><dd class="level2">';
}
add_shortcode( 'level1', 'level_first' );

function level_second( $atts, $content = null ){
	extract( shortcode_atts( array(
		'link' => '', 
	), $atts ) );

	$c = do_shortcode( $content );
	return '<dd class="level2"><a href="'.$link.'">'.$c.'</a><dd class="level2">';
}
add_shortcode( 'level2', 'level_second' );

function level_three( $atts, $content = null ){
	extract( shortcode_atts( array(
		'link' => '', 
	), $atts ) );

	$c = do_shortcode( $content );
	return '<dd class="level3"><a href="'.$link.'">'.$c.'</a><dd class="level2">';
}
add_shortcode( 'level3', 'level_three' );
 

/**
*	informaton
*/ 

function info_shortcode( $atts, $content = null ) { 
	return '<div class="uk-grid margin-content padding-page">'.do_shortcode( $content ).'</div>';
}
add_shortcode( 'info', 'info_shortcode' );

function info_l_shortcode( $atts, $content = null ) {

 	extract( shortcode_atts( array(
		'image' => '', 
	), $atts ) );

 	$html = ' 
        <div class="uk-width-large-1-2">
            <div class="uk-thumbnail info uk-thumbnail-expand">                 
                <img src="'.$image.'">
            </div>
        </div>
        <div class="uk-width-large-1-2 content">
        	'.do_shortcode( $content ).'
        </div>';

	return $html;
}
add_shortcode( 'left', 'info_l_shortcode' );

function info_r_shortcode( $atts, $content = null ) {

 	extract( shortcode_atts( array(
		'image' => '', 
	), $atts ) );

 	$html = ' 
        <div class="uk-width-large-1-2 content">
        	'.do_shortcode( $content ).'
        </div>
        <div class="uk-width-large-1-2">
            <div class="uk-thumbnail info uk-thumbnail-expand">                 
                <img src="'.$image.'">
            </div>
        </div>';

	return $html;
}
add_shortcode( 'right', 'info_r_shortcode' );

/**
*	Fasility
*/

function fasility_l_shortcode( $atts, $content = null ) {

 	extract( shortcode_atts( array(
		'image' => '', 
	), $atts ) );

 	$html = ' 
        <div class="uk-width-large-1-2">
			<div class="uk-thumbnail info uk-thumbnail-expand">                 
    			<img src="'.$image.'">
    		</div>
    	</div>
    	<div class="uk-width-large-1-2 content">
    		<ul class="paragraph-bullet">
            	'.do_shortcode( $content ).'   
            </ul>
    	</div>';

	return $html;
}
add_shortcode( 'left_fasility', 'fasility_l_shortcode' );

function fasility_r_shortcode( $atts, $content = null ) {

 	extract( shortcode_atts( array(
		'image' => '', 
	), $atts ) );

 	$html = ' 
 		<div class="uk-width-large-1-2 content">
    		<ul class="paragraph-bullet">
            	'.do_shortcode( $content ).'   
            </ul>
    	</div>
        <div class="uk-width-large-1-2">
			<div class="uk-thumbnail info uk-thumbnail-expand">                 
    			<img src="'.$image.'">
    		</div>
    	</div>';

	return $html;
}
add_shortcode( 'right_fasility', 'fasility_r_shortcode' );

/**
*	daftar harga
*/ 
function daftar_shortcode( $atts, $content = null ) {
 	extract( shortcode_atts( array(
		'image' => '', 
	), $atts ) );

 	$html = ' 
        <div class="uk-width-large-1-3">
			<div class="uk-thumbnail info uk-thumbnail-expand">                 
    			<img src="'.$image.'">
    		</div>
    		<div class="thumb-description">
    			'.do_shortcode( $content ).'
    		</div>
    	</div>';

	return $html;
}
add_shortcode( 'col3', 'daftar_shortcode' );

function daftar2_shortcode( $atts, $content = null ) {
 	extract( shortcode_atts( array(
		'image' => '', 
		'title'	=> '',
	), $atts ) );

 	$html = ' 
        <div class="uk-width-large-1-3 phone-daftar">                
    		<img src="'.$image.'"> <span class="font-medium">'.$title.' </span> 
    	</div>';

	return $html;
}
add_shortcode( 'phone', 'daftar2_shortcode' );

function daftar3_shortcode( $atts, $content = null ) {
 	extract( shortcode_atts( array( 
		'title'	=> '',
	), $atts ) );

 	$html = ' 
        <div class="uk-width-large-1-2 phone-daftar-description">     
    		<h2>'.$title.'</h2>
    		<ul class="font-medium">
    			'.do_shortcode($content).'
    		</ul>
    	</div>';

	return $html;
}
add_shortcode( 'col2', 'daftar3_shortcode' );

function daftar4_shortcode( $atts, $content = null ) {
 	extract( shortcode_atts( array( 
		'title'	=> '',
	), $atts ) );

 	$html = ' 
        <div class="uk-width-large-1-2 phone-daftar-description">     
    		<h2>'.$title.'</h2> 
    		'.do_shortcode($content).' 
    	</div>';

	return $html;
}
add_shortcode( 'col2span', 'daftar4_shortcode' );

function span_sort( $atts, $content = null ){
	$c = do_shortcode( $content );
	return '<p class="font-medium"><span>'.$c.'</span></p>';
}
add_shortcode( 'span', 'span_sort' );

function daftar5_shortcode( $atts, $content = null ) {
 	extract( shortcode_atts( array( 
		'title'	=> '',
	), $atts ) );

 	$html = ' 
        <div class="uk-width-large-1-1 phone-daftar-description number">     
    		<h2>'.$title.'</h2>
    		<ul class="font-medium">
    			'.do_shortcode($content).'
    		</ul>
    	</div>';

	return $html;
}
add_shortcode( 'col1', 'daftar5_shortcode' );


/**
*	daftar rekanan
*/ 
function rekanan_shortcode( $atts, $content = null ) {
 	return '<div class="uk-grid uk-grid-medium-30 margin-content-rekanan padding-page-contact rekanan">'.do_shortcode( $content ).'</div>';
}
add_shortcode( 'rekanan', 'rekanan_shortcode' );

function thumb4_shortcode( $atts, $content = null ) {

 	extract( shortcode_atts( array(
		'image' => 'http://dummyimage.com/207x207/000/fff', 
	), $atts ) );

 	$html = ' 
        <div class="uk-width-large-1-4 uk-width-medium-1-2 margin-rekanan">
            <div class="uk-thumbnail info uk-thumbnail-expand">                 
                <img src="'.$image.'">
            </div>
        </div>';

	return $html;
}
add_shortcode( 'thumb4', 'thumb4_shortcode' );


/**
*	Gallery
*/ 
function col2_gallery( $atts, $content = null ) { 

	extract( shortcode_atts( array(
		'title' => '', 
	), $atts ) );

	return ' 
	<div class="uk-grid margin-content padding-page"> 
        <div class="uk-width-large-1-1"> <h3> '.$title.' </h3></div> 
	</div>
	<div class="uk-grid uk-grid-medium-20 padding-page-img">
		'.do_shortcode($content).' 
    </div>
	';
}
add_shortcode( 'col2_gallery', 'col2_gallery' );

function gl2_gallery( $atts, $content = null ) { 
	return ' 
	<div class="uk-width-large-1-2">
        <div class="uk-thumbnail info uk-thumbnail-expand">                 
        	<img src="'.do_shortcode( $content ).'">
        </div> 
    </div> 
	';
}
add_shortcode( 'gl2', 'gl2_gallery' );


function col3_gallery( $atts, $content = null ) { 
	return '<div class="uk-grid uk-grid-medium-20 margin-content-gallery padding-page">'.do_shortcode( $content ).'</div>';
}
add_shortcode( 'col3_gallery', 'col3_gallery' );

function gl3_gallery( $atts, $content = null ) { 
	return ' 
	<div class="uk-width-large-1-3">
        <div class="uk-thumbnail info uk-thumbnail-expand">                 
        	<img src="'.do_shortcode( $content ).'">
        </div> 
    </div> 
	';
}
add_shortcode( 'gl3', 'gl3_gallery' );


function info_image( $atts, $content = null ){ 
	$c = do_shortcode( $content );
	return ''.$c.'';
}
add_shortcode( 'text', 'info_image' );



remove_filter( 'the_content', 'wpautop' ); 

//add_filter( 'the_content', 'wpautop' , 99);   
//add_filter( 'the_content', 'no_unautop_shortcodes',100 );
//add_filter( 'the_content', 'do_shortcode', 11 );

?>
 