<?php
function caption_shortcode( $atts, $content = null ) {

 	extract( shortcode_atts( array(
		'title' => '', 
	), $atts ) );

 	$html = '
 		<div class="uk-width-large-1-2 phone-daftar-description">     
    		<h2>'.$title.'</h2>
    		<ul class="font-medium">
    		'.do_shortcode($content).'
    		</ul>
    	</div>';

	return $html;
}
add_shortcode( 'one', 'caption_shortcode' );

function li_sort( $atts, $content = null ){
	$c = do_shortcode( $content );
	return '<li>'.$c.'</li>';
}
add_shortcode( 'li', 'li_sort' );

/**
*	Bread Crumb
*/
function breadcrumb_shortcode( $atts, $content = null ) {

 	extract( shortcode_atts( array(
		'title' => '', 
	), $atts ) );

 	$html = '
 		<div class="uk-grid uk-grid-small margin-content padding-page">
		<div class="uk-width-large-1-4 uk-width-small-1 uk-width-medium-3-10 uk-text-center-small">
    		 
            '.do_shortcode( $content ).'

    	</div>
		<div class="uk-width-large-1-2 uk-width-small-1 uk-width-medium-7-10 uk-text-center">
			<h1>'.$title.'</h1>
    	</div>
    	<div class="uk-width-large-1-4 uk-width-small-1 uk-width-medium-7-10 uk-text-left">
			&nbsp;
    	</div>
	</div>';

	return $html;
}
add_shortcode( 'breadcrumb', 'breadcrumb_shortcode' );

function level_first( $atts, $content = null ){
	extract( shortcode_atts( array(
		'link' => '', 
	), $atts ) );

	$c = do_shortcode( $content );
	return '<dd class="level1"><a href="'.get_permalink().'">'.$c.'</a><dd class="level2">';
}
add_shortcode( 'level1', 'level_first' );

function level_second( $atts, $content = null ){
	extract( shortcode_atts( array(
		'link' => '', 
	), $atts ) );

	$c = do_shortcode( $content );
	return '<dd class="level2"><a href="'.$link.'">'.$c.'</a><dd class="level2">';
}
add_shortcode( 'level2', 'level_second' );

function level_three( $atts, $content = null ){
	extract( shortcode_atts( array(
		'link' => '', 
	), $atts ) );

	$c = do_shortcode( $content );
	return '<dd class="level3"><a href="'.$link.'">'.$c.'</a><dd class="level2">';
}
add_shortcode( 'level3', 'level_three' );
 

/**
*	informaton
*/ 

function info_shortcode( $atts, $content = null ) { 
	return '<div class="uk-grid margin-content padding-page">'.do_shortcode( $content ).'</div>';
}
add_shortcode( 'info', 'info_shortcode' );

function info_l_shortcode( $atts, $content = null ) {

 	extract( shortcode_atts( array(
		'image' => '', 
	), $atts ) );

 	$html = ' 
        <div class="uk-width-large-1-2">
            <div class="uk-thumbnail info uk-thumbnail-expand">                 
                <img src="'.$image.'">
            </div>
        </div>
        <div class="uk-width-large-1-2 content">'.do_shortcode( $content ).'</div>';

	return $html;
}
add_shortcode( 'left', 'info_l_shortcode' );

function info_r_shortcode( $atts, $content = null ) {

 	extract( shortcode_atts( array(
		'image' => '', 
	), $atts ) );

 	$html = ' 
        <div class="uk-width-large-1-2 content">
        	'.do_shortcode( $content ).'
        </div>
        <div class="uk-width-large-1-2">
            <div class="uk-thumbnail info uk-thumbnail-expand">                 
                <img src="'.$image.'">
            </div>
        </div>';

	return $html;
}
add_shortcode( 'right', 'info_r_shortcode' );

/**
*	Fasility
*/

function fasility_l_shortcode( $atts, $content = null ) {

 	extract( shortcode_atts( array(
		'image' => '', 
	), $atts ) );

 	$html = ' 
        <div class="uk-width-large-1-2">
			<div class="uk-thumbnail info uk-thumbnail-expand">                 
    			<img src="'.$image.'">
    		</div>
    	</div>
    	<div class="uk-width-large-1-2 content">
    		<ul class="paragraph-bullet">'.do_shortcode( $content ).'</ul>
    	</div>';

	return $html;
}
add_shortcode( 'left_fasility', 'fasility_l_shortcode' );

function fasility_r_shortcode( $atts, $content = null ) {

 	extract( shortcode_atts( array(
		'image' => '', 
	), $atts ) );

 	$html = ' 
 		<div class="uk-width-large-1-2 content">
    		<ul class="paragraph-bullet">'.do_shortcode( $content ).'</ul>
    	</div>
        <div class="uk-width-large-1-2">
			<div class="uk-thumbnail info uk-thumbnail-expand"><img src="'.$image.'"></div>
    	</div>';

	return $html;
}
add_shortcode( 'right_fasility', 'fasility_r_shortcode' );

/**
*	daftar harga
*/ 
function daftar_shortcode( $atts, $content = null ) {
 	extract( shortcode_atts( array(
		'image' => '', 
	), $atts ) );

 	$html = ' 
        <div class="uk-width-large-1-3">
			<div class="uk-thumbnail info uk-thumbnail-expand">                 
    			<img src="'.$image.'">
    		</div>
    		<div class="thumb-description"><p>'.do_shortcode( $content ).'</p></div>
    	</div>';

	return $html;
}
add_shortcode( 'col3', 'daftar_shortcode' );

function daftar2_shortcode( $atts, $content = null ) {
 	extract( shortcode_atts( array(
		'image' => '', 
		'title'	=> '',
	), $atts ) );

 	$html = ' 
        <div class="uk-width-large-1-3 phone-daftar">                
    		<img src="'.$image.'"> <span class="font-medium">'.$title.'</span> 
    	</div>';

	return $html;
}
add_shortcode( 'phone', 'daftar2_shortcode' );

function daftar3_shortcode( $atts, $content = null ) {
 	extract( shortcode_atts( array( 
		'title'	=> '',
	), $atts ) );

 	$html = ' 
        <div class="uk-width-large-1-2 phone-daftar-description">     
    		<h2>'.$title.'</h2>
    		<ul class="font-medium">'.do_shortcode($content).'</ul>
    	</div>';

	return $html;
}
add_shortcode( 'col2', 'daftar3_shortcode' );

function daftar4_shortcode( $atts, $content = null ) {
 	extract( shortcode_atts( array( 
		'title'	=> '',
	), $atts ) );

 	$html = ' 
        <div class="uk-width-large-1-2 phone-daftar-description">     
    		<h2>'.$title.'</h2> 
    		'.do_shortcode($content).' 
    	</div>';

	return $html;
}
add_shortcode( 'col2span', 'daftar4_shortcode' );

function span_sort( $atts, $content = null ){
	$c = do_shortcode( $content );
	return '<p class="font-medium"><span>'.$c.'</span></p>';
}
add_shortcode( 'span', 'span_sort' );

function daftar5_shortcode( $atts, $content = null ) {
 	extract( shortcode_atts( array( 
		'title'	=> '',
	), $atts ) );

 	$html = ' 
        <div class="uk-width-large-1-1 phone-daftar-description number">     
    		<h2>'.$title.'</h2>
    		<ul class="font-medium">'.do_shortcode($content).'</ul>
    	</div>';

	return $html;
}
add_shortcode( 'col1', 'daftar5_shortcode' );


/**
*	daftar rekanan
*/ 
function rekanan_shortcode( $atts, $content = null ) {
 	return '<div class="uk-grid uk-grid-medium-30 margin-content-rekanan padding-page-contact rekanan">'.do_shortcode( $content ).'</div>';
}
add_shortcode( 'rekanan', 'rekanan_shortcode' );

function thumb4_shortcode( $atts, $content = null ) {

 	extract( shortcode_atts( array(
		'image' => 'http://dummyimage.com/207x207/000/fff', 
        'link'  => ''
	), $atts ) );

 	$html = ' 
        <div class="uk-width-large-1-4 uk-width-medium-1-2 margin-rekanan">
            <div class="uk-thumbnail info uk-thumbnail-expand">                 
                <a href="'.$link.'"><img src="'.$image.'"></a>
            </div>
        </div>';

	return $html;
}
add_shortcode( 'thumb4', 'thumb4_shortcode' );


/**
*	Gallery
*/ 
function col2_gallery( $atts, $content = null ) { 

	extract( shortcode_atts( array(
		'title' => '', 
	), $atts ) );

	return ' 
	<div class="uk-grid margin-content padding-page"> 
        <div class="uk-width-large-1-1"> <h3> '.$title.' </h3></div> 
	</div>
	<div class="uk-grid uk-grid-medium-20 padding-page-img">'.do_shortcode($content).'</div>';
}
add_shortcode( 'col2_gallery', 'col2_gallery' );

function gl2_gallery( $atts, $content = null ) { 

    extract( shortcode_atts( array(
        'link' => '', 
    ), $atts ) );

	return ' 
	<div class="uk-width-large-1-2">
        <div class="uk-thumbnail info uk-thumbnail-expand gallery-link">                 
            <a href="'.$link.'"><img src="'.do_shortcode( $content ).'"></a>
        </div> 
    </div> 
	';
}
add_shortcode( 'gl2', 'gl2_gallery' );


function col3_gallery( $atts, $content = null ) { 
	return '<div class="uk-grid uk-grid-medium-20 margin-content-gallery padding-page">'.do_shortcode( $content ).'</div>';
}
add_shortcode( 'col3_gallery', 'col3_gallery' );

function gl3_gallery( $atts, $content = null ) { 

    extract( shortcode_atts( array(
        'link' => '', 
    ), $atts ) );

	return ' 
	<div class="uk-width-large-1-3">
        <div class="uk-thumbnail info uk-thumbnail-expand gallery-link">                 
            <a href="'.$link.'"><img src="'.do_shortcode( $content ).'"></a>
        </div> 
    </div> 
	';
}
add_shortcode( 'gl3', 'gl3_gallery' );

/* home */
function home_shortcode( $atts, $content = null ) {

 	extract( shortcode_atts( array(
		'image' => '',
		'text'	=> '',
		'link'	=> '' 
	), $atts ) );

 	$html = '<div class="uk-grid margin-content">
		<div class="uk-width-large-3-10 uk-width-small-1 uk-width-medium-3-10 uk-text-center-small">
    		<img src="'.$image.'" class="uk-responsive-height">
    	</div>
		<div class="uk-width-large-7-10 uk-width-small-1 uk-width-medium-7-10">
			<div class="uk-vertical-align" style="height: 200px;">
			    <div class="uk-vertical-align-middle">
			    	<p>'.$text.'</p>
   					<div class="uk-text-right">
   						<a href="'.$link.'" class="btn-readmore"></a>
   					</div>
			    </div>
			</div>
    	</div>
	</div>';

	return $html;
}
add_shortcode( 'home', 'home_shortcode' );


/* contact */

function contact_shortcode( $atts, $content = null ) { 

	extract( shortcode_atts( array(
		'map'   => '', 
        'attr'  => 'width="540" height="400" frameborder="0" style="border:0"'
	), $atts ) );

	$html = null;
	$html .= '
  	<div class="uk-grid uk-grid-small margin-content padding-page-contact">
        <div class="uk-width-large-4-10 uk-width-medium-4-10 uk-width-small-1">
 	';

 	$html .= do_shortcode( $content );

 	$html .= '
    	</div>

        <div class="uk-width-large-6-10 uk-width-medium-4-10 uk-width-small-1 uk-text-right uk-text-center-small">
            <div class="">
                <iframe src="'.$map.'" '.$attr.'></iframe>
            </div>
            <div class="popup-success-box" id="popup-success-box">
               <div class="popup-success">
                   <div class="font-medium">Pertanyaan anda telah kami terima</div>
                   <a href="" class="ok-button"></a>
                </div>
            </div>
        </div>
	</div>
    ';

	return $html;
}
add_shortcode( 'contact', 'contact_shortcode' );

function contact_row_shortcode( $atts, $content = null ) {

 	extract( shortcode_atts( array(
		'image' => '',
		'text'	=> '', 
	), $atts ) );

 	$html  = '<div class="uk-grid uk-grid-small contact-phone">
                <div class="uk-width-medium-1-4 uk-width-small-1-4"> <img src="'.$image.'"> </div>
                <div class="uk-width-medium-2-4 uk-width-small-2-4">
                    <span class="font-small">'.$text.'</span> 
                </div>
            </div>';

	return $html;
}
add_shortcode( 'row-contact', 'contact_row_shortcode' );

function contact_form_shortcode( $atts, $content = null ) {

 	$html = '<div class="margin-content">'.do_shortcode($content).'</div>';

	return $html;
}
add_shortcode( 'form', 'contact_form_shortcode' );

function contact_form_dis_shortcode( $atts, $content = null ) {


 	$html = '

 	<form class="uk-form uk-form-horizontal sasana-form" method="POST" action="'.THEME_URI.'/send.php">
 
                                <div class="uk-form-row">
                                    <label class="uk-form-label" for="form-h-it">Name:</label>
                                    <div class="uk-form-controls name">
                                        <input type="text" placeholder="" name="name" id="name">
                                    </div>
                                </div>
                                <div class="uk-form-row">
                                    <label class="uk-form-label" for="form-h-ip">Email:</label>
                                    <div class="uk-form-controls email">
                                        <input type="text" placeholder="" name="email" id="email">
                                    </div>
                                </div>
                                <div class="uk-form-row">
                                    <label class="uk-form-label" for="form-h-ip">Handphone:</label>
                                    <div class="uk-form-controls handphone">
                                        <input type="text" placeholder="" name="handphone" id="handphone">
                                    </div>
                                </div> 
                                <div class="uk-form-row">
                                    <label class="uk-form-label" for="form-h-t">Pesanan:</label>
                                    <div class="uk-form-controls">
                                        <textarea cols="30" rows="5" placeholder="Textarea text" name="pesanan" id="pesanan"></textarea>
                                    </div>
                                </div>
                                <div class="uk-form-row">
                                    <label class="uk-form-label">Capcha:</label>
                                    <div class="uk-form-controls">
                                          <div class="uk-grid uk-grid-medium">
                                            <div class="uk-width-medium-8-10"> 
                                                <div class="uk-grid uk-grid-small">
                                                    <div class="uk-width-medium-1-2 uk-width-small-1-1">
                                                         <img src="'.THEME_URI.'/captcha/captcha-bak.php" id="captcha" alt="CAPTCHA code" width="120">
                                                         <small><a href="#captcha" id="reload">refresh</a></small></p>   <span class="error-captcha"></span>
                                                    </div>
                                                    <div class="uk-width-medium-1-2 uk-width-small-1-1 uk-text-right uk-text-left-small">
                                                        <div class="uk-vertical-align uk-panel" style="height: 47px;">
                                                            <div class="uk-vertical-align-middle">
                                                                <input type="text" name="captcha" id="captcha" placeholder="" class="uk-form-small">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> 
                                            </div>
                                            <div class="uk-width-medium-2-10 uk-text-right uk-text-left-small">
                                                <div class="uk-vertical-align uk-panel" style="height: 55px;">
                                                    <div class="uk-vertical-align-middle">
                                                        <button class="uk-button icon-button" name="submit"></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>   

                </form>';

	return $html;
}
add_shortcode( 'CONTACT_FORM', 'contact_form_dis_shortcode' );


/* CCalendar */

function calendar_shortcode( $atts, $content = null ) { 

 	$html = '<div class="uk-grid uk-grid-small margin-content padding-page grid-calendar">
	<div class="uk-width-large-3-10">'.do_shortcode( $content ).'</div>';

	return $html;
}
add_shortcode( 'balroom', 'calendar_shortcode' );

function calendar_kapas_shortcode( $atts, $content = null ) { 

	extract( shortcode_atts( array(
		'title' => '', 
	), $atts ) );

 	$html = '<div class="uk-grid uk-grid-small cal-sidebar">
             	<div class="uk-width-small-1 uk-width-medium-1 uk-text-left uk-text-center-small">
                    <h3><strong>'.$title.'</strong></h3>
            	</div>
            	'.do_shortcode( $content ).'
            </div>';

	return $html;
}
add_shortcode( 'kapasitas', 'calendar_kapas_shortcode' );

function calendar_text_shortcode( $atts, $content = null ) { 

	extract( shortcode_atts( array(
		'class' => '', 
	), $atts ) );

 	$html = '<div class="uk-width-small-1-2 uk-width-medium-1-2 uk-text-'.$class.' uk-text-center-small cal-sidebar-text">'.do_shortcode( $content ).'</div>';

	return $html;
}
add_shortcode( 'row', 'calendar_text_shortcode' );

function calendar_schedule_shortcode( $atts, $content = null ) { 

	extract( shortcode_atts( array(
		'image' => '', 
	), $atts ) );

	$html = null;
 	$html .= '
 			<div class="uk-grid uk-grid-medium margin-calendar-mmc">
                <div class="uk-width-small-1 uk-width-medium-1 uk-text-left uk-text-center-small">
                    <img src="'.$image.'">
                </div>

                 <div class="uk-width-small-1 uk-width-medium-1 uk-text-left uk-text-center-small margin-content">
                    &nbsp;
                </div>';

    $html .= do_shortcode( $content );

    $html .= '</div> ';

	return $html;
}
add_shortcode( 'schedule', 'calendar_schedule_shortcode' );

function calendar_confirm_shortcode( $atts, $content = null ) { 

	extract( shortcode_atts( array(
		'title' => '', 
	), $atts ) );

 	$html = '
 		<div class="uk-width-small-1-2 uk-width-medium-2-10 uk-text-left uk-text-center-small cal-sidebar-text">
             <div class="m-confirm night-top"></div>
        </div>
        <div class="uk-width-small-1-2 uk-width-medium-8-10 uk-text-left uk-text-center-small cal-sidebar-text h2-vertical-align">
        	<span class="h2-cal">'.$title.'</span>
        </div>
 	';

	return $html;
}
add_shortcode( 'm-confirm', 'calendar_confirm_shortcode' );

function calendar_confirm_t_shortcode( $atts, $content = null ) { 

	extract( shortcode_atts( array(
		'title' => '', 
	), $atts ) );

 	$html = '
 		<div class="uk-width-small-1-2 uk-width-medium-2-10 uk-text-left uk-text-center-small cal-sidebar-text">
             <div class="m-tentative night-top"></div>
        </div>
        <div class="uk-width-small-1-2 uk-width-medium-8-10 uk-text-left uk-text-center-small cal-sidebar-text h2-vertical-align">
        	<span class="h2-cal">'.$title.'</span>
        </div>
 	';

	return $html;
}
add_shortcode( 'm-tentative', 'calendar_confirm_t_shortcode' );

function calendar_c_shortcode( $atts, $content = null ) { 

	extract( shortcode_atts( array(
		'title' => '', 
	), $atts ) );

 	$html = '
 		<div class="uk-width-small-1-2 uk-width-medium-2-10 uk-text-left uk-text-center-small cal-sidebar-text">
             <div class="c-confirm night-top"></div>
        </div>
        <div class="uk-width-small-1-2 uk-width-medium-8-10 uk-text-left uk-text-center-small cal-sidebar-text h2-vertical-align">
        	<span class="h2-cal">'.$title.'</span>
        </div>
 	';

	return $html;
}
add_shortcode( 'c-confirm', 'calendar_c_shortcode' );

function calendar_c_t_shortcode( $atts, $content = null ) { 

	extract( shortcode_atts( array(
		'title' => '', 
	), $atts ) );

 	$html = '
 		<div class="uk-width-small-1-2 uk-width-medium-2-10 uk-text-left uk-text-center-small cal-sidebar-text">
             <div class="c-tentative night-top"></div>
        </div>
        <div class="uk-width-small-1-2 uk-width-medium-8-10 uk-text-left uk-text-center-small cal-sidebar-text h2-vertical-align">
        	<span class="h2-cal">'.$title.'</span>
        </div>
 	';

	return $html;
}
add_shortcode( 'c-tentative', 'calendar_c_t_shortcode' );


function info_image( $atts, $content = null ){ 
	$c = do_shortcode( $content );
	return ''.$c.'';
}
add_shortcode( 'text', 'info_image' );



add_filter( 'the_content', 'tgm_io_shortcode_empty_paragraph_fix' );
function tgm_io_shortcode_empty_paragraph_fix( $content ) {
 
    $str = preg_replace('~<p>\s*<\/p>~i','', $content);

    
    $source = array( '<br />', '<p></p>' );
    $replace = array( '', '' );

    $str = str_replace( $source, $replace, $content );
    return $str;
 
}

//remove_filter( 'the_content', 'wpautop' ); 

//add_filter( 'the_content', 'wpautop' , 99);   
//add_filter( 'the_content', 'no_unautop_shortcodes',100 );
//add_filter( 'the_content', 'do_shortcode', 11 );

?>