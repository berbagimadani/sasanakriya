<?php
add_action( 'init', 'create_post_type' );
function create_post_type() {
    register_post_type( 'slider_item',
        array(
            'labels' => array(
                'name' => 'Slider Items',
                'singular_name' => 'Slider Item',
                'add_item' =>  'New Slider Item',
                'add_new_item' => 'Add New Slider Item',
                'edit_item' =>  'Edit Slider Item' 
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'slider'),
            'menu_position' => 5,
            'show_ui' => true,
            'supports' => array('author', 'title', 'editor', 'thumbnail')
            
        )
    );
}


add_action( 'init', 'create_slider_category_taxonomies', 0 );
function create_slider_category_taxonomies() 
{
  // Add new taxonomy, make it hierarchical (like categories)
  $labels = array(
    'name' => __( 'Slider Categories', 'taxonomy general name' ),
    'singular_name' => __( 'Slider Category', 'taxonomy singular name' ),
    'search_items' =>  'Search Slider Categories',
    'all_items' => 'All Slider Categories',
    'parent_item' => 'Parent Slider Category',
    'parent_item_colon' => 'Parent Slider Category:',
    'edit_item' => 'Edit Slider Category', 
    'update_item' => 'Update Slider Category' ,
    'add_new_item' => 'Add New Slider Category',
    'new_item_name' => 'New Slider Category Name',
    'menu_name' => 'Slider Categories',
  );     

  register_taxonomy('slider_category',array('slider_item'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'slider_category' ),
  ));
}

add_filter('manage_edit-slider_item_columns', 'add_new_slider_item_columns');

function add_new_slider_item_columns($slider_item_columns) {
    $new_columns['cb'] = '<input type="checkbox" />';

    $new_columns['title'] = __('Slider Item', 'column name');
    
    $new_columns['author'] = 'Author';

    $new_columns['slider_category'] = 'Slider Categories';
    $new_columns['tags'] = 'Tags';
    
    $new_columns['thumb'] = 'Thumb';

    $new_columns['date'] = __('Date', 'column name');

    return $new_columns;
}

// Add to admin_init function
add_action('manage_slider_item_posts_custom_column', 'manage_slider_item_columns', 10, 2);

function manage_slider_item_columns($column_name, $id) {
    global $wpdb;
    switch ($column_name) {

    case 'slider_category':
        // Get number of images in gallery
        echo get_the_term_list( $id, 'slider_category', '', ', ', '' );
        break;    
        
    case 'thumb':
        echo the_post_thumbnail( array(100, 60) );
        break;
    default:
        break;
    } // end switch
}
?>