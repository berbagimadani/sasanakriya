<?php
/*-----------------------------------------------------------------------------------*/
/*	Add Metaboxes
/*-----------------------------------------------------------------------------------*/ 

add_action( 'load-post.php', 'genthemes_meta_boxes_setup' );
add_action( 'load-post-new.php', 'genthemes_meta_boxes_setup' );

/* Meta box setup function. */
if(!function_exists('genthemes_meta_boxes_setup')) :
function genthemes_meta_boxes_setup() {
	global $typenow;
	if($typenow == 'page'){
		add_action( 'add_meta_boxes', 'genthemes_load_page_metaboxes' );
		add_action( 'save_post', 'genthemes_save_page_metaboxes', 10, 2);
	}
}
endif;

/* Add page metaboxes */
if(!function_exists('genthemes_load_page_metaboxes')) :
	function genthemes_load_page_metaboxes() {
			
			 
		  /* Social share metabox */
		 //if(opt_genthemes('opt_button_sharing') != '0'){
		  add_meta_box(
		    'genthemes_short_desc',
		    __('Choose slider page for header',THEME_SLUG),
		    'genthemes_social_share_metabox',
		    'page',
		    'normal',
		    'default'
		  );
		 //}
		  
	}
endif;

/* Create Short Description Metabox */
if(!function_exists('genthemes_social_share_metabox')) :
	function genthemes_social_share_metabox($object, $box) {
	  global $pagenow; 
	  wp_nonce_field( __FILE__ , 'genthemes_page_nonce' ); 
	  
	  $defaults = array('slider_page' => '');
	  $genthemes_meta = get_post_meta($object->ID,'_genthemes_meta',true);
	  $genthemes_meta = wp_parse_args( (array) $genthemes_meta, $defaults );
 
	  ?>
	  	<p> 
	  		
	  	<?php   
				
				$args_cat = array(
									'type'                     => 'slider_item',
									'child_of'                 => 0,
									'parent'                   => '',
									'orderby'                  => 'name',
									'order'                    => 'DESC',
									'hide_empty'               => 1,
									'hierarchical'             => 1,
									'exclude'                  => '',
									'include'                  => '',
									'number'                   => '',
									'taxonomy'                 => 'slider_category',
									'pad_counts'               => false 
								
				);  
			  ?>
			  <select name="genthemes[slider_page]">
			  <option value="">Choose page slider</option>
			  <?php   
				$categories = get_categories( $args_cat );
				foreach ($categories as $tag){  
					?>
					<option value="<?php echo $tag->slug; ?>" <?php selected( $tag->slug, $genthemes_meta['slider_page'] );?>><?php echo $tag->name; ?></option>
					<?php
				}
			  ?>  
			  </select>

	  	</p>
			
	  <?php
	}
endif;

 
/* Save Page Meta */
if(!function_exists('genthemes_save_page_metaboxes')) :
	function genthemes_save_page_metaboxes($post_id, $post ) {
		  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
		    return;
		    
		  if(isset($_POST['genthemes_page_nonce'])){
		  	if ( !wp_verify_nonce( $_POST['genthemes_page_nonce'], __FILE__  ) )
		    	return;
		  }
		   
		  if($post->post_type == 'page' && isset($_POST['genthemes'])) {
		  	$post_type = get_post_type_object( $post->post_type );
		  	if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
		    return $post_id;
		    
		    $slider_page = isset($_POST['genthemes']['slider_page']) ? $_POST['genthemes']['slider_page'] : ''; 
		  	 
			$genthemes_meta = array(
				'slider_page' => $slider_page, 
			);
				
		  	update_post_meta($post_id, '_genthemes_meta', $genthemes_meta); 
			}
	}
endif;

 

 
?>