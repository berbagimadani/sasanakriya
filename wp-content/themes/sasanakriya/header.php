<?php 
/**
* Gen Themes Display.
* @package WordPress 
* @subpackage Genthemes V1
* @since genthemes v1
* @web genthemes.net
* @email genthemes@gmail.com
*/
?>
<!doctype html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo opt_genthemes('favicon');?>" >
    <link rel="apple-touch-icon" href="<?php echo opt_genthemes('favicon');?>" >
    <title><?php bloginfo().wp_title( '|', true, 'left' ); ?></title>
    

  <?php wp_head(); ?> 
  <?php $root = get_template_directory_uri(); ?>  
  <?php echo opt_genthemes('google_analytic');?>

    
  </head>

<body <?php body_class(); ?>> 
 
<div class="uk-container uk-container-center base-container">
  <!--  MENU -->
    
    <div class="uk-grid">
      <div class="uk-width-1">
        <div class=""> 

          <?php include('menu.php'); ?> 

        </div>
      </div>
  </div>

  <!-- SLIDER -->

  <div class="uk-grid margin-slider">
      <div class="uk-width-1">

        <?php if( is_front_page() ) { ?>
        <div class="uk-panel- uk-panel-box-">
        
        <!-- Flipster List -->  
          <div class="flipster">
            <ul>

             <?php 
                  
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                $args = array(
                  'post_type' => 'slider_item',
                  'slider_category' =>  'home',
                  'paged'=> $paged,
                  'order' => 'DESC',
                  'posts_per_page' => '20'
                );  
                $the_query = new WP_Query( $args ); 
                while ($the_query->have_posts()):
                  $the_query->the_post();   
                  $featured_image_array = wp_get_attachment_image_src( get_post_thumbnail_id(), 'single-post-thumbnail' );

                  $featured_image = $featured_image_array[0];
                ?>  
                        
                <li> 
                <a href="#">  
                  <img src="<?php echo $featured_image; ?>"> 
                </a>
                </li>
                <?php endwhile;?> 

            </ul>
          </div>
        <!-- End Flipster List -->
        </div>
        <?php } else { ?>

        <?php if ( sasanakriya_meta('slider_page') ) { ?>
        <div> 
          <ul class="uk-slideshow" data-uk-slideshow="{autoplay:true}">
           <?php 
                  
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                $args = array(
                  'post_type' => 'slider_item',
                  'slider_category' =>  sasanakriya_meta('slider_page'),
                  'paged'=> $paged,
                  'order' => 'DESC',
                  'posts_per_page' => '20'
                );  
                $the_query = new WP_Query( $args ); 
                while ($the_query->have_posts()):
                  $the_query->the_post();   
                  $featured_image_array = wp_get_attachment_image_src( get_post_thumbnail_id(), 'single-post-thumbnail' );

                  $featured_image = $featured_image_array[0];
                ?>  
                        
                <li>   
                  <img src="<?php echo $featured_image; ?>">  
                </li>
                <?php endwhile;?> 
          </ul>
        </div>
        <?php } ?>
        <?php } ?>


      </div>
  </div>

  
  <!-- Footer -->
  
</body>
</html>