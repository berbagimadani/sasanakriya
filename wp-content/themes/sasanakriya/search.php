<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>
<div class="row">
      <div class="large-12 column mar-top-right-30">
         
      </div>
      <div class="large-8 medium-8 small-12 column">
		<?php if ( have_posts() ) : ?>
 			<h3 class="page-title"><?php printf( __( 'Search Results for: %s', 'genthemesrocia' ), '<span>' . get_search_query() . '</span>' ); ?></h3>
		 

			<?php genthemesblog_content_nav( 'nav-below' ); ?>

			<?php /* Start the Loop */ ?> 
			<div class="row row-pad">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?> 
			</div>

			<?php genthemesblog_content_nav( 'nav-below' ); ?>
			

		<?php else : ?>
 				<article id="post-0" class="post no-results not-found">
					<header class="entry-header">
						<h1 class="entry-title"><?php _e( 'Nothing Found', 'genthemesrocia' ); ?></h1>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'twentyeleven' ); ?></p>
						<?php get_search_form(); ?>
					</div><!-- .entry-content -->
				</article><!-- #post-0 -->

		<?php endif; ?>
		</div>
 
<?php get_sidebar(); ?>
<?php get_footer(); ?>