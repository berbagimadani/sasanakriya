<?php
/**
 * Gen Themes Display.
 * @package WordPress
 * @subpackage Genthemes V1
 * @since genthemes v1
 * @web genthemes.net
 * @email genthemes@gmail.com
 */
get_header(); ?> 


<?php if( is_home() ) { ?>

<!-- CONTENT -->
  
  <?php the_content(); ?>

  <div class="uk-grid margin-content">
    <div class="uk-width-large-3-10 uk-width-small-1 uk-width-medium-3-10 uk-text-center-small">
        <img src="images/logo.jpg" class="uk-responsive-height">
      </div>
    <div class="uk-width-large-7-10 uk-width-small-1 uk-width-medium-7-10">
      <div class="uk-vertical-align" style="height: 200px;">
          <div class="uk-vertical-align-middle">
            <p>
          Sasana Kriya dengan gaya minimalis modern merupakan salah satu pilihan tempat terbaik untuk pernikahan, peluncuran produk, pameran, konferensi, konvensi, wisuda atau pertemuan lainnya. Dengan banyak pilihan rekanan rekanan katering, dekorasi dan ruang parkir yang luas, Sasana Kriya adalah pilihan utama untuk acara Anda
            </p>
            <div class="uk-text-right">
              <a href="#" class="btn-readmore"></a>
            </div>
          </div>
      </div>
      </div>
  </div>

  <!-- LINE -->
  <div class="uk-grid margin-content">
    <div class="uk-width-large-1">
        <hr class="line">
      </div>
  </div>

<?php } ?>

<?php get_footer(); ?>