<div class="uk-grid margin-content-footer">

    <div class="uk-width-large-1-3 uk-width-medium-1-3">
        <div class="uk-text-right uk-text-center-small uk-margin-top uk-push-1-5">
          <a href="<?php echo opt_genthemes('link_fb');?>"><img src="<?php echo opt_genthemes('fb');?>"></a>
        </div>
      </div>
      <div class="uk-width-large-1-3 uk-width-medium-1-3">
        <div class="uk-text-center">
           <a href="<?php echo opt_genthemes('link_tmii');?>"><img src="<?php echo opt_genthemes('tmii');?>"></a>
        </div>
      </div>
      <div class="uk-width-large-1-3 uk-width-medium-1-3">
        <div class="uk-text-left uk-text-center-small uk-margin-top uk-pull-1-5">
          <a href="<?php echo opt_genthemes('link_weeding');?>"><img src="<?php echo opt_genthemes('weeding');?>"></a>
        </div>
      </div>

  </div>

  <div class="uk-grid">

    <div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1">
        <div class="uk-text-left uk-text-center-small">
          <h5><?php echo opt_genthemes('copyright');?></h5>
        </div>
      </div>
      <div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1">
        <div class="uk-text-right uk-text-center-small design-by">
          <h5><?php echo opt_genthemes('design_by');?></h5>
        </div>
      </div>

  </div>



</div>
  
   
  <?php wp_footer(); ?>
  </body>
</html>
