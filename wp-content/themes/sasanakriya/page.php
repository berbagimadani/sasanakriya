<?php
/**
 * Gen Themes Display.
 * @package WordPress
 * @subpackage Genthemes V1
 * @since genthemes v1
 * @web genthemes.net
 * @email genthemes@gmail.com
 */

get_header(); ?>  
<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'content', 'page' ); ?>
	
<?php endwhile; // end of the loop. ?>
 
<?php //wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'genthemesv1' ), 'after' => '</div>' ) ); ?> 
<?php //get_sidebar(); ?>
<?php get_footer(); ?>