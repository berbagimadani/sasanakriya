<?php
//session_start(); 

/**
 * Gen Themes functions and definitions.
 * @package WordPress
 * @subpackage Genthemes V1
 * @since genthemes v1
 * @web genthemes.net
 * @email genthemes@gmail.com
 */ 
 
define('THEME_DIR', get_template_directory());
define('THEME_URI', get_template_directory_uri());
define('THEME_NAME', 'Sasana Kriya');
define('THEME_SLUG', 'sasanakriya');
define('THEME_VERSION', '1.1.0');
define('THEME_OPTIONS','genthemes_settings');
 
//require_once( '/include/options.php' );
//require_once( dirname(__FILE__). '/include/sample-config.php' );
//require_once(dirname(__FILE__).'/include/sample-config-news.php');

/* ------------------------------------------------------------------------- *
 *  Load theme files
/* ------------------------------------------------------------------------- */	
if ( ! function_exists( 'sasanakriya_load' ) ) {
	function sasanakriya_load() {  
		
		load_template ( get_stylesheet_directory() . '/theme-options/genthemes_net_options.php');
		load_template ( get_stylesheet_directory() . '/include/functions/walker-menu.php');
		load_template ( get_stylesheet_directory() . '/include/functions/walker-uikit.php');

		load_template ( get_stylesheet_directory() . '/include/functions/slider_functions.php');
		load_template ( get_stylesheet_directory() . '/include/functions/shortcode_custome.php'); 

		load_template ( get_stylesheet_directory() . '/include/functions/metaboxes.php'); 

    	load_theme_textdomain( 'sasanakriya', get_template_directory() . '/languages' );
		 
	}
}
add_action( 'after_setup_theme', 'sasanakriya_load' );

/* ------------------------------------------------------------------------- *
 *  Base functionality
/* ------------------------------------------------------------------------- */

// Content width
if ( !isset( $content_width ) ) { $content_width = 620; }

/*  Theme setup
/* ------------------------------------ */
if ( ! function_exists( 'sasanakriya_setup' ) ) {
	function sasanakriya_setup() {		
		// Enable automatic feed links
		add_theme_support( 'automatic-feed-links' );
		
		// Enable featured image
		add_theme_support( 'post-thumbnails' );
		
		// Enable post format support
		add_theme_support( 'post-formats', array( 'gallery', 'image', 'link', 'video' ) );
		
		// Declare WooCommerce support
		add_theme_support( 'woocommerce' );
		
		// Thumbnail sizes
		add_image_size( 'thumb-small', 160, 160, true );
		add_image_size( 'thumb-medium', 520, 245, true );
		add_image_size( 'thumb-large', 720, 340, true );
 
	} 
}
add_action( 'after_setup_theme', 'sasanakriya_setup' );


/* ------------------------------------------------------------------------- *
 *  Enqueue Styles & javascript
/* ------------------------------------------------------------------------- */
if ( ! function_exists( 'sasanakriya_scripts_styles' ) ) {
	function sasanakriya_scripts_styles() {
		$root = get_template_directory_uri();
		
		wp_enqueue_style( 'uikit-min-css', $root . '/css/uikit.min.css', array(), false);
		wp_enqueue_style( 'base-css', $root . '/css/base.css', array(), false);

		/* componsne */
		wp_enqueue_style( 'slide-css', $root . '/css/components/slideshow.css', array(), false);
		wp_enqueue_style( 'slider-css', $root . '/slider/src/css/jquery.flipster.css', array(), false); 
		
		wp_deregister_script('jquery');
		//wp_register_script('jquery', ("https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"), false, '1.9.1', true);
		wp_register_script( 'jquery', $root . '/js/jquery.js', array(), false, true );
		
		wp_enqueue_script( 'uikit-min-js', $root . '/js/uikit.min.js', array('jquery'), false, true );
		wp_enqueue_script( 'slider-js', $root . '/slider/src/js/jquery.flipster.js', array('jquery'), false, true ); 
		wp_enqueue_script( 'flipster-js', $root . '/slider/src/js/flipster.js', array('jquery'), false, true ); 
		wp_enqueue_script( 'slide-js', $root . '/js/components/slideshow.js', array('jquery'), false, true);
		wp_enqueue_script( 'contact-js', $root . '/js/contact.js', array('jquery'), false, true);
	}
}
add_action( 'wp_enqueue_scripts', 'sasanakriya_scripts_styles' );



/*  Display On Button Social Share
/* ------------------------------------ */
if ( ! function_exists( 'sasanakriya_meta' ) ) {
	function sasanakriya_meta($obj){
		global $post;
		$defaults = array('slider_page' => '');
	  	$genthemes_meta = get_post_meta($post->ID,'_genthemes_meta',true);
	  	$genthemes_meta = wp_parse_args( (array) $genthemes_meta, $defaults );
	  	return $genthemes_meta[$obj];
	}
}


/* Get theme option function 
/* ------------------------------------ */
if ( ! function_exists( 'opt_genthemes' ) ) { 
	function opt_genthemes( $id ){ 
		$options = get_option( 'genthemesfr_options' ); 

		if( isset($options[$id]) ){
			return $options[$id];
		}
	} 
	 
} 

 

/*  Pagination
 */
/* ------------------------------------ */	
if ( ! function_exists( 'sasanakriya_page_num' ) ) {
	function sasanakriya_page_num( $html_id ) {
		global $wp_query;   
		if ( $wp_query->max_num_pages > 1 ) : ?>
		<ul class="pagination">
			<?php 
					$big = 999999999; // need an unlikely integer 
					echo paginate_links( array(
						'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
						'format' => '?paged=%#%',
						'current' => max( 1, get_query_var('paged') ),
						'total' => $wp_query->max_num_pages
					) ); 
			?>
		</ul>
		<?php endif;
	} 
}

/*  View post
 */
/* ------------------------------------ */	
if ( ! function_exists( 'wpb_set_post_views' ) ) {
	function wpb_set_post_views($postID) {
	    $count_key = 'wpb_post_views_count';
	    $count = get_post_meta($postID, $count_key, true);
	    if($count==''){
	        $count = 0;
	        delete_post_meta($postID, $count_key);
	        add_post_meta($postID, $count_key, '0');
	    }else{
	        $count++;
	        update_post_meta($postID, $count_key, $count);
	    }
	}
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

if ( ! function_exists( 'view_count' ) ) {
	function view_count($postID){
	    $count_key = 'wpb_post_views_count';
	    $count = get_post_meta($postID, $count_key, true);
	    if($count==''){
	        delete_post_meta($postID, $count_key);
	        add_post_meta($postID, $count_key, '0');
	        return "0";
	    }
	    return $count;
	}
}
if ( ! function_exists( 'wpb_track_post_views' ) ) {
	function wpb_track_post_views ($post_id) {
	    if ( !is_single() ) return;
	    if ( empty ( $post_id) ) {
	        global $post;
	        $post_id = $post->ID;    
	    }
	    wpb_set_post_views($post_id);
	}
}
add_action( 'wp_head', 'wpb_track_post_views'); 


// Helper for SC TinyMCE button
function sc_admin_footer(){

	if (isset($_GET['page']) && $_GET['page'] == 'shortcoder')
		return;
	
	echo "
<script>
window.onload = function(){
	if( typeof QTags === 'function' )
		QTags.addButton( 'QT_sc_insert', 'Shortcoder', sc_show_insert );
}
function sc_show_insert(){
	tb_show('Insert a Shortcode', '" . THEME_URI . "/js/sc-insert.php?TB_iframe=true');
}
</script>
";
}
add_action('admin_footer', 'sc_admin_footer');

/**
*	Shortcoder tinyMCE buttons
*/

function sc_addbuttons() { 

	//if ( get_user_option('rich_editing') == 'true') {
		add_filter("mce_external_plugins", "sc_add_tinymce_plugin");
		add_filter('mce_buttons', 'sc_register_button');
	//}
}
 
function sc_register_button($buttons) {
   array_push($buttons, "separator", "scbutton");
   return $buttons;
}

function sc_add_tinymce_plugin($plugin_array) {
   $plugin_array['scbutton'] = THEME_URI . '/js/tinymce/editor_plugin.js';
   return $plugin_array;
}
add_action('init', 'sc_addbuttons'); // init process for button control


/*-----------------------------------------------------------------------------------*/
/*	Theme Includes
/*-----------------------------------------------------------------------------------*/
 
add_filter( 'wp_mail_from', function( $email ) {
    return $_POST['email'];
});

add_filter( 'wp_mail_from_name', function( $name ) {
    return 'Sasanakriya Email System';
});
 