<?php
/**
 * Shortcoder include for inserting and editing shortcodes in post and pages
 * v1.2
 **/
 
if ( ! isset( $_GET['TB_iframe'] ) )
	define( 'IFRAME_REQUEST' , true );


// Load WordPress
require_once('../../../../wp-load.php');

if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') )
    wp_die(__('You do not have permission to edit posts.'));
 
		
?>

<html>
<head>
<title>Shortcodes created</title> 
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.js"></script>

<style type="text/css">
body{
	font: 13px Arial, Helvetica, sans-serif;
	padding: 10px;
	background: #f2f2f2;
}
h2{
	font-size: 23px;
	font-weight: normal;
}
h4{
	margin: 0 0 20px 0;
}
hr{
	border-width: 0px;
	margin: 15px 0;
	border-bottom: 1px solid #DFDFDF;
}
.sc_wrap{
	border: 1px solid #DFDFDF;
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.1);
}
.sc_shortcode{
	border-bottom: 1px solid #CCC;
	padding: 0px;
	background: #FFF;
}
.sc_shortcode_name{
	cursor: pointer;
	padding: 10px;
}
.sc_shortcode_name:hover{
	background: #fbfbfb;
}
.sc_params{
	border: 1px solid #DFDFDF;
	background: #F9F9F9;
	margin: 0 -1px -1px;
	padding: 20px;
	display: none;
}
.sc_insert{
	background: linear-gradient(to bottom, #09C, #0087B4);
	color: #FFF;
	padding: 5px 15px;
	border: 1px solid #006A8D;
	font-weight: bold;
}

.sc_insert:hover{
	opacity: 0.8;
}
input[type=text], textarea{
	padding: 5px;
	border: 1px solid #CCC;
	width: 120px;
	margin: 0px 25px 10px 0px;
}
.sc_toggle{
	background: url(images/toggle-arrow.png) no-repeat;
	float: right;
	width: 16px;
	height: 16px;
	opacity: 0.4;
}

.sc_share_iframe{
	background: #FFFFFF;
	border: 1px solid #dfdfdf;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	-moz-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.1);
	-webkit-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.1);
	box-shadow: 0 1px 5px rgba(0, 0, 0, 0.1);
}
.sc_credits{
	background: url(images/aw.png) no-repeat;
	padding-left: 23px;
	color: #8B8B8B;
	margin-left: -5px;
	font-size: 13px;
	text-decoration: none;
}
</style>
<script type="text/javascript">
$(document).ready(function(){ 
	$('.sc_shortcode_name').append('<span class="sc_toggle"></span>');
	
	$('.sc_insert').click(function(){
		
		var params = '';
		var scname = $(this).attr('data-name');
		var sc = '';
		
		$(this).parent().children().find('input[type="text"]').each(function(){
			if($(this).val() != ''){
				attr = $(this).attr('data-param');
				val = $(this).val();
				params += attr + '=' + val + ' ';
			}
		});
		
		if(wsc(scname)){
			name = '' + scname + '';
		}else{
			name = scname;
		}
		sc = '' + name + ' ' + params + '';
		
		if( typeof parent.send_to_editor !== undefined ){
			parent.send_to_editor(sc);
		}
		
	});
	
	$('.sc_share_bar img').mouseenter(function(){
		$this = $(this);
		$('.sc_share_iframe').remove();
		$('body').append('<iframe class="sc_share_iframe"></iframe>');
		$('.sc_share_iframe').css({
			position: 'absolute',
			top: $this.offset()['top'] - $this.attr('data-height') - 15,
			left: $this.offset()['left'] - $this.attr('data-width')/2 ,
			width: $this.attr('data-width'),
			height: $this.attr('data-height'),
		}).attr('src', $this.attr('data-url')).hide().fadeIn();
	
	});
	
	$('.sc_shortcode_name').click(function(e){
		$('.sc_params').slideUp();
		if($(this).next('.sc_params').is(':visible')){
			$(this).next('.sc_params').slideUp();
		}else{
			$(this).next('.sc_params').slideDown();
		}
	})
	
});

var sc_closeiframe = function(){
	$('.sc_share_iframe').remove();
}

function wsc(s){
	if(s == null)
		return '';
	return s.indexOf(' ') >= 0;
}
</script>
</head>
<body> 
<h2> Insert shortcode to editor</h2>

<div class="sc_wrap">
<?php

$breadcrumb ="
[breadcrumb title='Title Page'] <br>
[level1] Level 1 [/level1] <br>
[level2 link=] Level 2  [/level2] <br>
[level3 link=] Leevel 3 [/level3] <br>
[/breadcrumb]"; 

$information ="
[info] <br>

[left image = 'http://lorempixel.com/400/200/'] <br>
[text]Lorem Ipsum is[/text] <br>
[/left] <br>

[right image = 'http://lorempixel.com/400/200/'] <br>
[text]Lorem Ipsum is[/text] <br>
[/right] <br>

[/info]

";

$daftar ="
[info]<br>

[col3 image='http://Lorem Ipsum is']<br>
Lorem Ipsum is <br>
[/col3]<br>

[col3 image='http://Lorem Ipsum is']<br>
Lorem Ipsum is <br>
[/col3]<br>

[col3 image='http://....'] <br>
Lorem Ipsum is <br>
[/col3] <br>

[phone image='http://....' title='Lorem Ipsum is'] <br>
[phone image='http://....' title='Lorem Ipsum is'] <br>
[phone image='http://....' title='Lorem Ipsum is'] <br>

[col2 title='LOREM IPSUM:'] <br>
	[li]Lorem Ipsum is[/li] <br>
	[li]Lorem Ipsum is[/li] <br>
[/col2] <br>
[col2 title='LOREM IPSUM:'] <br>
	[li]Lorem Ipsum is[/li] <br>
	[li]Lorem Ipsum is[/li] <br>
[/col2] <br>

[col2 title='LOREM IPSUM:'] <br>
	[li]Lorem Ipsum is[/li] <br>
	[li]Lorem Ipsum is[/li] <br>
[/col2] <br>

[col2span title='LOREM IPSUM:'] <br>
	[span]Lorem Ipsum is[/span]  <br>
[/col2span] <br>

[col1 title='LOREM IPSUM:'] <br>
	[li]Lorem Ipsum is[/li] <br>
	[li]Lorem Ipsum is[/li] <br>
	[li]Lorem Ipsum is[/li] <br>
	[li]Lorem Ipsum is[/li] <br>
[/col1] <br>


[/info] 

";
 
$rekanan = "
[rekanan] <br>
[thumb4 image='http://' LINK='http://'][/thumb4] <br>
[thumb4 image='http://' LINK='http://'][/thumb4] <br>
[thumb4 image='http://' LINK='http://'][/thumb4] <br>
[thumb4 image='http://' LINK='http://'][/thumb4] <br>
[/rekanan] <br>
";

$fasility = "
[info] <br>

[left_fasility image='http://'] <br>
[li]Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod[/li] <br>
[li]Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod[/li] <br>
[li]Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod[/li] <br>
[li]Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod[/li] <br>
[/left_fasility] <br>

[right_fasility image='http://'] <br>
[li]Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod[/li] <br>
[li]Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod[/li] <br>
[li]Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod[/li] <br>
[li]Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod[/li] <br>
[/right_fasility] <br>

[/info] <br>
";


$home ="
[home image='http://...' link='http://....' text='Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod' ]
[/home]
";

$contact ="

[contact map='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15862.778669636298!2d106.88594774634377!3d-6.303789057020346!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69ed5dcbc72743%3A0xb419f460a3c1c93!2sSasana+Kriya!5e0!3m2!1sen!2s!4v1394250543397' attr='width=540 height=400 frameborder=0 style=border:0'] <br>
[row-contact image='http://...' text='Lorem ipsum dolor sit amet, consectetur'] <br>

[row-contact image='http://...' text='Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod'] <br>

[row-contact image='http://...' text='Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod'] <br>

[form]  <br>

[CONTACT_FORM]  <br>

[/form]<br>
[/contact]
";

$balroom ="
[balroom]

[kapasitas title='Lorem ipsum dolor'] <br>

[row class='left'] Lorem ipsum dolor [/row] <br>

[row class='right'] 400 [/row] <br>

[row class='left'] Lorem ipsum dolor [/row] <br>

[row class='right'] 900 [/row] <br>

[/kapasitas] <br>

[schedule image='http://..'] <br>

[m-confirm title ='Lorem ipsum dolor'] <br>

[m-tentative title ='Lorem ipsum dolor'] <br>

[c-confirm title ='Lorem ipsum dolor'] <br>

[c-tentative title ='Lorem ipsum dolor'] <br>

[/schedule]

[/balroom]
";

$gallery = "
[col2_gallery title ='Lorem ipsum dolor sit amet'] <br>
[gl2 link='http://....'] http://.... [/gl2] <br>
[gl2 link='http://....'] http://.... [/gl2] <br>
[/col2_gallery] <br>

[col3_gallery] <br>
[gl3 link='http://....'] http://.... [/gl3] <br>
[gl3 link='http://....'] http://.... [/gl3] <br>
[gl3 link='http://....'] http://.... [/gl3] <br>
[/col3_gallery] <br>

[col2_gallery title ='Lorem ipsum dolor sit amet'] <br>
[gl2 link='http://....'] http://.... [/gl2] <br>
[gl2 link='http://....'] http://.... [/gl2] <br>
[/col2_gallery] <br>

";

$sc_options = array (
	'breadcrumb'	=> $breadcrumb,
	'home'			=> $home,  
	'information'	=> $information,
	'fasility'		=> $fasility,  
	'daftar'		=> $daftar,  
	'rekanan'		=> $rekanan,
	'gallery'		=> $gallery,
	'hubungi'		=> $contact,
	'balroom'		=> $balroom,
);


foreach($sc_options as $key=>$value){ 
		//echo '<div class="sc_shortcode"><div class="sc_shortcode_name">' . $key;
		//echo '</div>';
		//echo $value;
		echo '<input type="button" class="sc_insert cupid-blue" data-name="' . $value . '" value="Insert Shortcode '. $key.'"/><br>';
		//echo '</div>';
		//echo '</div>'; 
}
?>
</div>
 

</body>
</html>