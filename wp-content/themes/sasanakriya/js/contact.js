 $(document).ready(function() {

    $("#popup-success-box").hide();

     

     $('#reload').click(function(){
        var d = new Date();    
        var t = $("#captcha").attr('src');                  
        $('img#captcha').attr('src', t+'?' + Math.random() );
    });

    // process the form
    $('form').submit(function(event) {

        // get the form data
        // there are many ways to get this data using jQuery (you can use the class or id also)
        var formData = {
            'name'              : $('input[name=name]').val(),
            'email'             : $('input[name=email]').val(),
            'handphone'         : $('input[name=handphone]').val(),
            'pesanan'           : $('textarea[name=pesanan]').val(),
            'captcha'           : $('input[name=captcha]').val(),
        };

        // process the form
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : $(this).attr('action'), // the url where we want to POST
            data        : formData, // our data object
            dataType    : 'json', // what type of data do we expect back from the server
                        encode          : true
        })
            // using the done promise callback
            .done(function(data) {

                // log data to the console so we can see
                console.log(data); 

                
                if ( ! data.success) {
            
                    $('.uk-form-controls.name').next().remove();
                    if (data.errors.name) {
                        $('#name').addClass('uk-form-danger'); // add the error class to show red input
                        //$('.uk-form-controls.name').parent().append('<div class="uk-alert uk-alert-danger">' + data.errors.name + '</div>'); // add the actual error message under our input
                    }

                    $('.uk-form-controls.email').next().remove();
                    if (data.errors.email) {
                        $('#email').addClass('uk-form-danger'); // add the error class to show red input
                        //$('.uk-form-controls.email').parent().append('<div class="uk-alert uk-alert-danger">' + data.errors.email + '</div>');
                    }
 
                    if (data.errors.handphone) {
                        $('#handphone').addClass('uk-form-danger'); // add the error class to show red input
                    }

                    if (data.errors.pesanan) {
                        $('#pesanan').addClass('uk-form-danger'); // add the error class to show red input
                    }

                    if (data.errors.captcha) {
                        $('input[name=captcha]').addClass('uk-form-danger'); // add the error class to show red input
                        $('.error-captcha').html('<small>' + data.errors.captcha + '</small>'); // add the actual error message under our input
                    } else{
                        $('input[name=captcha]').removeClass('uk-form-danger'); 
                         $('.error-captcha').html('<small></small>'); 
                    }


                } else {

                    // ALL GOOD! just show the success message!
                    $("#popup-success-box").fadeIn('slow').show();
                    emptyValues();

                }


            });

        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
    });

});

function showValues() {
    var fields = $( ":input" ).serializeArray();
    
    $( "#results" ).empty();
    jQuery.each( fields, function( i, field ) {
        $( "#results" ).append( field.value + " " );
    });
}

function emptyValues() {
    var fields = $( ":input" ).serializeArray();
    $('button[name=submit]').attr('disabled', true); 
    $( "#results" ).empty();
    jQuery.each( fields, function( i, field ) {
        $( "#"+field.name ).removeClass('uk-form-danger'); 

        $( "input[name='"+field.name+"']" ).val('');
        $( "textarea[name='"+field.name+"']" ).val('');
        $( "#"+field.name).attr('disabled', true);

    });
    $('#results').append('<div class="uk-alert uk-alert-success">success</div>'); 
}