<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'sasanakriya');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'x #>krHr[CBHf+9LX6x)hJ/llZTacZ_zr,fEE2;6^|d-86f[$gaqTiFbmnliTM+n');
define('SECURE_AUTH_KEY',  '*Dq_x0&}m}=GI3ml}dxNi4D#-MK{Mh}gY!|cF<vI@7Ag,%->afh|qttf.|!YaNd8');
define('LOGGED_IN_KEY',    'h_}ee}|2rBv|]|-trG_9y@l14bB<p07;.5G.D(7eSM`As Tp|i?,OSqXf*PVoP.p');
define('NONCE_KEY',        'DSo]d+sYez~@v*F8-xS=88uS!gP*4clWi?dq3,#(O:;%/2-a%::xH85Cg+R&dF?-');
define('AUTH_SALT',        '.WyxYPtXy8McOO!p>yDNHNf!*Kr^U5<0p[LSC`zN))wX,#o+D6v4<ERqwdWf3c(E');
define('SECURE_AUTH_SALT', '}$+|dc$Gt4..6t;,7_}AOp^3Rd5^KJ$tZ+71gwLo7Dw,w%g-q?g&g@q!^?HvxYQp');
define('LOGGED_IN_SALT',   'UbrP+,bx9619r(5Y-?V_j1U_5<@nJ--C+`vOZu_6K-r2)#D.Q65yu6fvL8j.7u^:');
define('NONCE_SALT',       'VFOWh48YtLUo$h<I  8;AktIMupal=NhTgF<@y*-Vl,ze98<DU9NIC_ru0_3x[$R');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
