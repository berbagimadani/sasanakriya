<!DOCTYPE html>
<html>
    <head>
    	<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="A layout example that shows off a responsive photo gallery.">
        <title></title>
        <link rel="stylesheet" href="css/uikit.min.css" />
        <link rel="stylesheet" href="css/base.css" />

        <!--- CSS Componen -->
        <link rel="stylesheet" href="css/components/slideshow.css" />

        <script src="js/jquery.js"></script>
        <script src="js/uikit.min.js"></script>


        <!-- JS componen -->
        <script src="js/components/slideshow.js"></script> 

        <!-- Slider Cover Flow -->
        <link rel="stylesheet" href="slider/css/demo.css---">
   		<link rel="stylesheet" href="slider/src/css/jquery.flipster.css">
    	<link rel="stylesheet" href="slider/css/flipsternavtabs.css--">


    	<script type="text/javascript">
    	 //$(function(){ $(".uk-slideshow").slideshow({ height: '200px' }); }); ....
    	</script>
    </head>
    <body>

<div class="uk-container uk-container-center">
	<!--  MENU -->
    
    <div class="uk-grid base">
    	<div class="uk-width-1">
    		<div class="">
    			
    			<div>
    				
    				<?php include('menu.php'); ?>

    			</div>

    		</div>
    	</div>
	</div>

	<!-- SLIDER -->

	<div class="uk-grid margin-slider">
    	<div class="uk-width-1">
    		<div class="">
    			<ul class="uk-slideshow" data-uk-slideshow="{autoplay:true}">
    				<li><img src="images/Ceiling.png"></li>
    				<li><img src="images/Doorway.png"></li>
    				<li><img src="images/Frontdoor.png"></li>
    			</ul>
    		</div>
    	</div>
	</div>

	<!-- CONTENT -->

	<div class="uk-grid uk-grid-small margin-content padding-page">
		<div class="uk-width-large-1-4 uk-width-small-1 uk-width-medium-3-10 uk-text-center-small uk-margin-top">
    		<!--<h4>Informasi <dd style="padding-left:20px">Hubungi</dd> </h4>-->

    	</div>
		<div class="uk-width-large-1-2 uk-width-small-1 uk-width-medium-7-10 uk-text-center">
			<h1> Calendar Ballroom </h1>
    	</div>
    	<div class="uk-width-large-1-4 uk-width-small-1 uk-width-medium-7-10 uk-text-left">
			&nbsp;
    	</div>
	</div>

	<div class="uk-grid uk-grid-small margin-content padding-page grid-calendar">
		
		<div class="uk-width-large-3-10">
		  
            <div class="uk-grid uk-grid-small cal-sidebar">
                <div class="uk-width-small-1 uk-width-medium-1 uk-text-left uk-text-center-small">
                    <h3><strong>Kapasitas</strong></h3>
                </div>
                <div class="uk-width-small-1-2 uk-width-medium-1-2 uk-text-left uk-text-center-small cal-sidebar-text">Grand Ballroom </div>
                <div class="uk-width-small-1-2 uk-width-medium-1-2 uk-text-right uk-text-center-small cal-sidebar-text">max 3000</div>
                <div class="uk-width-small-1-2 uk-width-medium-1-2 uk-text-left uk-text-center-small cal-sidebar-text">Mandira  </div>
                <div class="uk-width-small-1-2 uk-width-medium-1-2 uk-text-right uk-text-center-small cal-sidebar-text">max 3000- 100</div>
                <div class="uk-width-small-1-2 uk-width-medium-1-2 uk-text-left uk-text-center-small cal-sidebar-text">Mandira </div>
                <div class="uk-width-small-1-2 uk-width-medium-1-2 uk-text-right uk-text-center-small cal-sidebar-text">max 3000</div>
            </div>
            
                
            <div class="uk-grid uk-grid-medium margin-calendar-mmc">
                
                <div class="uk-width-small-1 uk-width-medium-1 uk-text-left uk-text-center-small">
                    <img src="images/symbol-cal.png">
                </div>

                 <div class="uk-width-small-1 uk-width-medium-1 uk-text-left uk-text-center-small margin-content">
                    &nbsp;
                </div>

                <div class="uk-width-small-1-2 uk-width-medium-2-10 uk-text-left uk-text-center-small cal-sidebar-text">
                    <div class="m-confirm night-top"></div>
                </div>
                <div class="uk-width-small-1-2 uk-width-medium-8-10 uk-text-left uk-text-center-small cal-sidebar-text h2-vertical-align">
                    <span class="h2-cal">Mandira Confirmed</span>
                </div>


                <div class="uk-width-small-1-2 uk-width-medium-2-10 uk-text-left uk-text-center-small cal-sidebar-text">
                    <div class="m-tentative night-top"></div>
                </div>
                <div class="uk-width-small-1-2 uk-width-medium-8-10 uk-text-left uk-text-center-small cal-sidebar-text h2-vertical-align">
                    <span class="h2-cal">Mandira Confirmed</span>
                </div>


                <div class="uk-width-small-1-2 uk-width-medium-2-10 uk-text-left uk-text-center-small cal-sidebar-text">
                    <div class="c-confirm night-top"></div>
                </div>
                <div class="uk-width-small-1-2 uk-width-medium-8-10 uk-text-left uk-text-center-small cal-sidebar-text h2-vertical-align">
                    <span class="h2-cal">Mandira Confirmed</span>
                </div>

                <div class="uk-width-small-1-2 uk-width-medium-2-10 uk-text-left uk-text-center-small cal-sidebar-text">
                    <div class="c-tentative night-top"></div>
                </div>
                <div class="uk-width-small-1-2 uk-width-medium-8-10 uk-text-left uk-text-center-small cal-sidebar-text h2-vertical-align">
                    <span class="h2-cal">Mandira Confirmed</span>
                </div>   

            </div> 

    	</div> 
        
        <!-- CALENDAR -->
        <div class="uk-width-large-7-10 uk-calendar-right">
            <div class="uk-grid uk-grid-small uk-calendar-right">  
                <div class="uk-width-large-1-1">
                    
                     
                    <div class="uk-grid">
                            <div class="uk-width-large-1-3 uk-width-medium-1-3 uk-width-small-1-3">
                                <div class="uk-grid uk-grid-collapse">
                                    <div class="uk-width-large-1-5 uk-width-medium-1-5 uk-width-small-1-5 uk-text-left"><a href="#" class="cal-arrow-left"></a></div>
                                    <div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1-2 uk-text-center uk-cal-ym"><span class="cal-ym"></span></div>
                                    <div class="uk-width-large-1-5 uk-width-medium-1-5 uk-width-small-1-5 uk-text-right"><a href="#" class="cal-arrow-right"></a></div>
                                </div>
                            </div>
                            <div class="uk-width-large-1-3 uk-width-medium-1-3 uk-width-small-1-3">
                                &nbsp;
                            </div>
                            <div class="uk-width-large-1-3 uk-width-medium-1-3 uk-width-small-1-3 uk-text-right">
                                <div class="uk-grid uk-grid-collapse">
                                    <div class="uk-width-large-1-5 uk-width-medium-1-5 uk-width-small-1-5 uk-text-left"><a href="#" class="cal-arrow-left"></a></div>
                                    <div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1-2 uk-text-center uk-cal-ym"><span class="cal-ym"></span></div>
                                    <div class="uk-width-large-1-5 uk-width-medium-1-5 uk-width-small-1-5 uk-text-right"><a href="#" class="cal-arrow-right"></a></div>
                                </div>
                            </div>
                    </div>
                    

                
                    <div class="uk-grid uk-grid-small margin-calendar">
                        <table >
                          <tr class="daily-head">
                            <td>Minggu</td>
                            <td>Senin</td>
                            <td>Selasa</td>
                            <td>Rabu</td>
                            <td>Kamis</td>
                            <td>Jum'at</td>
                            <td>Sabtu</td>
                          </tr>
                          <tr class="daily">
                            <td class="daily-bg"></td>
                            <td class="daily-bg"></td>
                            <td class="daily-bg"> 
                                <div class="daily-box">
                                    <!--
                                    <div class="daily-box-one"><span class="daily-date">1</span></div>
                                    <div class="daily-box-two" ><span class="m-confirm-block"><div class="m-tentative night-1"></div></span></div>
                                    <div class="daily-box-tree">
                                        <span class="m-confirm-block"><div class="m-confirm night-top"></div></span>
                                        <span class="m-confirm-block"><div class="c-confirm night-bottom"></div></span>
                                    </div>
                                    <div class="daily-box-four"><span class="m-confirm-block"><div class="c-tentative night-2"></div></span></div>
                                    -->
                                </div>
                            </td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                          </tr>
                          <tr class="daily">
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                          </tr>
                          <tr class="daily">
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                          </tr>
                          <tr class="daily"> 
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                          </tr>
                          <tr class="daily">
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"></td>
                            <td class="daily-bg"></td>
                          </tr>
                          <tr class="daily">
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"><span class="daily-date"></span></td>
                            <td class="daily-bg"></td>
                            <td class="daily-bg"></td>
                          </tr>
                        </table> 
                    </div>
                </div>
            </div> 
        </div>
        <!-- // CALENDAR -->



	</div>


	<!-- LINE -->
	
	<div class="uk-grid margin-content">
		<div class="uk-width-large-1">
    		<hr class="line">
    	</div>
	</div>

	<!-- Footer -->
	
	<?php include('footer.php'); ?>

</body>
</html>