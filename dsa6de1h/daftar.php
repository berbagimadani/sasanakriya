<!DOCTYPE html>
<html>
    <head>
    	<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="A layout example that shows off a responsive photo gallery.">
        <title></title>
        <link rel="stylesheet" href="css/uikit.min.css" />
        <link rel="stylesheet" href="css/base.css" />

        <!--- CSS Componen -->
        <link rel="stylesheet" href="css/components/slideshow.css" />

        <script src="js/jquery.js"></script>
        <script src="js/uikit.min.js"></script>


        <!-- JS componen -->
        <script src="js/components/slideshow.js"></script> 

        <!-- Slider Cover Flow -->
        <link rel="stylesheet" href="slider/css/demo.css---">
   		<link rel="stylesheet" href="slider/src/css/jquery.flipster.css">
    	<link rel="stylesheet" href="slider/css/flipsternavtabs.css--">


    	<script type="text/javascript">
    	 //$(function(){ $(".uk-slideshow").slideshow({ height: '200px' }); });
    	</script>
    </head>
    <body>

<div class="uk-container uk-container-center">
	<!--  MENU -->
    
    <div class="uk-grid">
    	<div class="uk-width-1">
    		<div class="">
    			
    			<div>
    				
    				<?php include('menu.php'); ?>

    			</div>

    		</div>
    	</div>
	</div>

	<!-- SLIDER -->

	<div class="uk-grid margin-slider">
    	<div class="uk-width-1">
    		<div class="">
    			<ul class="uk-slideshow" data-uk-slideshow="{autoplay:true}">
    				<li><img src="images/Ceiling.png"></li>
    				<li><img src="images/Doorway.png"></li>
    				<li><img src="images/Frontdoor.png"></li>
    			</ul>
    		</div>
    	</div>
	</div>

	<!-- CONTENT -->

	<div class="uk-grid uk-grid-small margin-content padding-page">
		<div class="uk-width-large-1-4 uk-width-small-1 uk-width-medium-3-10 uk-text-center-small">
    		<dd class="level1"><a href="">Daftar Harga </a></dd>
            <dd class="level2"><a href="#">Grand Ball Room</a></dd>
            <dd class="level3"><a href="#">Pesta Pernikahan</a></dd>

    	</div>
		<div class="uk-width-large-1-2 uk-width-small-1 uk-width-medium-7-10 uk-text-center">
			<h1> Daftar Harga Ballroom </h1>
    	</div>

    	<div class="uk-width-large-1-4 uk-width-small-1 uk-width-medium-7-10 uk-text-left">
			&nbsp;
    	</div>
	</div>

	<div class="uk-grid margin-content-2 padding-page">
		
		<!-- IMAGE THUMBNAIL TEXT CAPTION -->

		<div class="uk-width-large-1-3">
			<div class="uk-thumbnail info uk-thumbnail-expand">                 
    			<img src="images/thumb-1.jpg">
    		</div>
    		<div class="thumb-description">
    			<p>
    				Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit
    			</p>
    		</div>
    	</div> 
    	<div class="uk-width-large-1-3">
    		<div class="uk-thumbnail info uk-thumbnail-expand">                 
    			<img src="images/thumb-1.jpg">
    		</div>
    		<div class="thumb-description">
    			<p>
    				Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit
    			</p>
    		</div>
    	</div>
    	<div class="uk-width-large-1-3">
    		<div class="uk-thumbnail info uk-thumbnail-expand">                 
    			<img src="images/thumb-1.jpg">
    		</div>
    		<div class="thumb-description">
    			<p>
    				Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit
    			</p>
    		</div>
    	</div>

    	<!-- /IMAGE THUMBNAIL TEXT CAPTION -->


    	<!-- PHONE -->

		<div class="uk-width-large-1-3 phone-daftar">                
    		<img src="images/phone.png"> <span class="font-medium">Lorem ipsum dolor sit </span> 
    	</div> 
    	<div class="uk-width-large-1-3 phone-daftar">
    		<img src="images/phone.png"> <span class="font-medium">Lorem ipsum dolor sit </span>  
    	</div>
    	<div class="uk-width-large-1-3 phone-daftar">
    		<img src="images/phone.png"> <span class="font-medium">Lorem ipsum dolor sit </span> 
    	</div>

    	<!-- /PHONE-->

    	<div class="uk-width-large-1-2 phone-daftar-description">     
    		<h2>BALLROOM CARANI KAPASITAS:</h2>
    		<ul class="font-medium">
    			<li>Pertemuan 400 – 500 orang</li>
    			<li>Pertemuan 400 – 500 orang</li>
    		</ul>
    	</div>

    	<div class="uk-width-large-1-2 phone-daftar-description">     
    		<h2>BALLROOM MANDIRA KAPASITAS:</h2>
    		<ul class="font-medium">
    			<li>Pertemuan 400 – 500 orang</li>
    			<li>Pertemuan 400 – 500 orang</li>
    		</ul>
    	</div>

    	<div class="uk-width-large-1-2 phone-daftar-description">     
    		<h2>WAKTU MANDIRA KAPASITAS:</h2>
    		<ul class="font-medium">
    			<li>Pertemuan 400 – 500 orang</li>
    			<li>Pertemuan 400 – 500 orang</li>
    		</ul>
    	</div>

    	<div class="uk-width-large-1-2 phone-daftar-description">     
    		<h2>PEMESANAN:</h2>
    		<p class="font-medium"><span>Pemesanan tempat dapat kami layani setiap hari Senin –
    		Minggu, pkl. 09.00 – 17.00 WIB.</span></p>
    	</div>

    	<div class="uk-width-large-1-1 phone-daftar-description number">     
    		<h2>PEMBAYARAN</h2>
    		<ul class="font-medium">
    			<li>
    				Uang muka sebesar Rp.5.500.000,- untuk penggunaan salah satu Ballroom Gedung Sasana Kriya.  Apabila pemakaian dua		    (Grand) Ballroom uang muka sebesar Rp 11.000.000,-. Dibayarkan paling lambat 7 (tujuh) hari sejak konfirmasi, apabila 			    telah melewati tenggat waktu yang diberikan (7 hari) maka pihak gedung berhak membatalkan pemesanan tanggal terse		    but.
    			</li>
    			<li>5 (lima) bulan sebelum acara pelunasan sewa gedung.</li>
    			<li>5 (lima) bulan sebelum acara pelunasan sewa gedung.</li>
    			<li>5 (lima) bulan sebelum acara pelunasan sewa gedung.</li>
    			<li>5 (lima) bulan sebelum acara pelunasan sewa gedung.</li>
    			<li>Cek mundur tidak dapat diterima sebagai alat pembayaran sewa gedung</li>
    		</ul>
    	</div>

	</div>


	<!-- LINE -->
	
	<div class="uk-grid margin-content">
		<div class="uk-width-large-1">
    		<hr class="line">
    	</div>
	</div>

	<!-- Footer -->
	
	<?php include('footer.php'); ?>

</body>
</html>