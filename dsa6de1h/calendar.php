<!DOCTYPE html>
<html>
    <head>
    	<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="A layout example that shows off a responsive photo gallery.">
        <title></title>
        <link rel="stylesheet" href="css/uikit.min.css" />
        <link rel="stylesheet" href="css/base.css" />

        <!--- CSS Componen -->
        <link rel="stylesheet" href="css/components/slideshow.css" />

        <script src="js/jquery.js"></script>
        <script src="js/uikit.min.js"></script>


        <!-- JS componen -->
        <script src="js/components/slideshow.js"></script> 

        <!-- Slider Cover Flow -->
        <link rel="stylesheet" href="slider/css/demo.css---">
   		<link rel="stylesheet" href="slider/src/css/jquery.flipster.css">
    	<link rel="stylesheet" href="slider/css/flipsternavtabs.css--">


    	<script type="text/javascript">
    	 //$(function(){ $(".uk-slideshow").slideshow({ height: '200px' }); }); ....
    	</script>
    </head>
    <body>

<div class="uk-container uk-container-center">
	 
        <!-- CALENDAR -->
        <div class="uk-width-large-7-10 uk-calendar-right">
            <div class="uk-grid uk-grid-small uk-calendar-right">  
                <div class="uk-width-large-1-1">
                    <div class="uk-grid uk-grid-small">
                            <div class="uk-width-large-1-3 uk-width-medium-1-3 uk-width-small-1-3">
                                <div class="uk-grid uk-grid-small">
                                    <div class="uk-width-large-1-3"><button>«</button></div>
                                    <div class="uk-width-large-1-3"><span>Tahun</span></div>
                                    <div class="uk-width-large-1-3"><button>«</button></div>
                                </div>
                            </div>
                            <div class="uk-width-large-1-3 uk-width-medium-1-3 uk-width-small-1-3">
                                &nbsp;
                            </div>
                            <div class="uk-width-large-1-3 uk-width-medium-1-3 uk-width-small-1-3 uk-text-right">
                                <div class="uk-grid uk-grid-small">
                                    <div class="uk-width-large-1-3"><button>«</button></div>
                                    <div class="uk-width-large-1-3"><span>BUlan</span></div>
                                    <div class="uk-width-large-1-3"><button>«</button></div>
                                </div>
                            </div>
                    </div>

                
                    <div class="uk-grid uk-grid-small margin-calendar">
                        <table >
                          <tr>
                            <td>Minggu</td>
                            <td>Senin</td>
                            <td>Selasa</td>
                            <td>Rabu</td>
                            <td>Kamis</td>
                            <td>JUmat</td>
                            <td>Sabtu</td>
                          </tr>
                          <tr class="daily">
                            <td class="daily-bg"></td>
                            <td class="daily-bg"></td>
                            <td class="daily-bg">1</td>
                            <td class="daily-bg">2</td>
                            <td class="daily-bg">3</td>
                            <td class="daily-bg">4</td>
                            <td class="daily-bg">5</td>
                          </tr>
                          <tr class="daily">
                            <td class="daily-bg">6</td>
                            <td class="daily-bg">7</td>
                            <td class="daily-bg">8</td>
                            <td class="daily-bg">9</td>
                            <td class="daily-bg">10</td>
                            <td class="daily-bg">11</td>
                            <td class="daily-bg">12</td>
                          </tr>
                          <tr class="daily">
                            <td class="daily-bg">13</td>
                            <td class="daily-bg">14</td>
                            <td class="daily-bg">15</td>
                            <td class="daily-bg">16</td>
                            <td class="daily-bg">17</td>
                            <td class="daily-bg">18</td>
                            <td class="daily-bg">19</td>
                          </tr>
                          <tr class="daily"> 
                            <td class="daily-bg">20</td>
                            <td class="daily-bg">21</td>
                            <td class="daily-bg">22</td>
                            <td class="daily-bg">23</td>
                            <td class="daily-bg">24</td>
                            <td class="daily-bg">25</td>
                            <td class="daily-bg">26</td>
                          </tr>
                          <tr class="daily">
                            <td class="daily-bg">27</td>
                            <td class="daily-bg">28</td>
                            <td class="daily-bg">29</td>
                            <td class="daily-bg">30</td>
                            <td class="daily-bg">31</td>
                            <td class="daily-bg"></td>
                            <td class="daily-bg"></td>
                          </tr>
                        </table> 
                    </div>
                </div>
            </div> 
        </div>
        <!-- // CALENDAR -->


	</div> 

</body>
</html>