<div class="uk-grid margin-content-footer">

		<div class="uk-width-large-1-3 uk-width-medium-1-3">
    		<div class="uk-text-right uk-text-center-small uk-margin-top uk-push-1-5">
    			<img src="images/icon-fb.png">
    		</div>
    	</div>
    	<div class="uk-width-large-1-3 uk-width-medium-1-3">
    		<div class="uk-text-center">
    			<img src="images/icon-tmii.jpg">
    		</div>
    	</div>
    	<div class="uk-width-large-1-3 uk-width-medium-1-3">
    		<div class="uk-text-left uk-text-center-small uk-margin-top uk-pull-1-5">
    			<img src="images/icon-wedd.jpg">
    		</div>
    	</div>

	</div>

	<div class="uk-grid">

		<div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1">
    		<div class="uk-text-left uk-text-center-small">
    			<h5>© Sasana Kriya 2013-2015</h5>
    		</div>
    	</div>
    	<div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1">
    		<div class="uk-text-right uk-text-center-small">
    			<h5>Dewi<b>Codex</b></h5>
    		</div>
    	</div>

	</div>



</div>
 <script src="js/jquery.popupoverlay.js"></script>
 
<script src="slider/src/js/jquery.flipster.js"></script>
	<script>
		<!--

			$(function(){ $(".flipster").flipster({ style: 'carousel', start: 0, enableNavButtons: 	true, }); });

		-->
		</script>

<script type="text/javascript">
    
    $(document).ready(function() {
    
     $("#popup-success-box").hide();

     $('#reload').click(function(){
        var d = new Date();                         
        $('img#captcha').attr('src', 'captcha/captcha-bak.php?' + Math.random() );
    });

   
    // process the form
    $('form').submit(function(event) {

        // get the form data
        // there are many ways to get this data using jQuery (you can use the class or id also)
        var formData = {
            'name'              : $('input[name=name]').val(),
            'email'             : $('input[name=email]').val(),
            'handphone'         : $('input[name=handphone]').val(),
            'pesanan'           : $('textarea[name=pesanan]').val(),
            'captcha'           : $('input[name=captcha]').val(),
        };

        // process the form
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : 'send.php', // the url where we want to POST
            data        : formData, // our data object
            dataType    : 'json', // what type of data do we expect back from the server
                        encode          : true
        })
            // using the done promise callback
            .done(function(data) {

                // log data to the console so we can see
                //console.log(data); 

                
                if ( ! data.success) {
            
                    $('.uk-form-controls.name').next().remove();
                    if (data.errors.name) {
                        $('#name').addClass('uk-form-danger'); // add the error class to show red input
                        //$('.uk-form-controls.name').parent().append('<div class="uk-alert uk-alert-danger">' + data.errors.name + '</div>'); // add the actual error message under our input
                    }

                    $('.uk-form-controls.email').next().remove();
                    if (data.errors.email) {
                        $('#email').addClass('uk-form-danger'); // add the error class to show red input
                        //$('.uk-form-controls.email').parent().append('<div class="uk-alert uk-alert-danger">' + data.errors.email + '</div>');
                    }
 
                    if (data.errors.handphone) {
                        $('#handphone').addClass('uk-form-danger'); // add the error class to show red input
                    }

                    if (data.errors.pesanan) {
                        $('#pesanan').addClass('uk-form-danger'); // add the error class to show red input
                    }

                    if (data.errors.captcha) {
                        $('input[name=captcha]').addClass('uk-form-danger'); // add the error class to show red input
                        $('.error-captcha').html('<small>' + data.errors.captcha + '</small>'); // add the actual error message under our input
                    } else{
                        $('input[name=captcha]').removeClass('uk-form-danger'); 
                         $('.error-captcha').html('<small></small>'); 
                    }

                    

                      

                } else {

                    // ALL GOOD! just show the success message!
                    $("#popup-success-box").fadeIn('slow').show();
                    emptyValues();


                }


            });

        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
    });

});

function showValues() {
    

    $('button[name=submit]').attr('disabled', true); 

    var fields = $( ":input" ).serializeArray();
    
    $( "#results" ).empty();
    jQuery.each( fields, function( i, field ) {
        $( "#results" ).append( field.name + " " );

        $( "#"+field.name).attr('disabled', true);

    });
}

function emptyValues() {
    var fields = $( ":input" ).serializeArray();
    $('button[name=submit]').attr('disabled', true); 
    $( "#results" ).empty();
    jQuery.each( fields, function( i, field ) {
        $( "#"+field.name ).removeClass('uk-form-danger'); 

        $( "input[name='"+field.name+"']" ).val('');
        $( "textarea[name='"+field.name+"']" ).val('');

        $( "#"+field.name).attr('disabled', true);

    });
    //$('#results').append('<div class="uk-alert uk-alert-success">success</div>'); 
}
 
</script>
