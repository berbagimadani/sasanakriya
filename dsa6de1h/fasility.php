<!DOCTYPE html>
<html>
    <head>
    	<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="A layout example that shows off a responsive photo gallery.">
        <title></title>
        <link rel="stylesheet" href="css/uikit.min.css" />
        <link rel="stylesheet" href="css/base.css" />

        <!--- CSS Componen -->
        <link rel="stylesheet" href="css/components/slideshow.css" />

        <script src="js/jquery.js"></script>
        <script src="js/uikit.min.js"></script>


        <!-- JS componen -->
        <script src="js/components/slideshow.js"></script> 

        <!-- Slider Cover Flow -->
        <link rel="stylesheet" href="slider/css/demo.css---">
   		<link rel="stylesheet" href="slider/src/css/jquery.flipster.css">
    	<link rel="stylesheet" href="slider/css/flipsternavtabs.css--">


    	<script type="text/javascript">
    	 //$(function(){ $(".uk-slideshow").slideshow({ height: '200px' }); }); ....
    	</script>
    </head>
    <body>

<div class="uk-container uk-container-center">
	<!--  MENU -->
    
    <div class="uk-grid base">
    	<div class="uk-width-1">
    		<div class="">
    			
    			<div>
    				
    				<?php include('menu.php'); ?>

    			</div>

    		</div>
    	</div>
	</div>

	<!-- SLIDER -->

	<div class="uk-grid margin-slider">
    	<div class="uk-width-1">
    		<div class="">
    			<ul class="uk-slideshow" data-uk-slideshow="{autoplay:true}">
    				<li><img src="images/Ceiling.png"></li>
    				<li><img src="images/Doorway.png"></li>
    				<li><img src="images/Frontdoor.png"></li>
    			</ul>
    		</div>
    	</div>
	</div>

	<!-- CONTENT -->

	<div class="uk-grid uk-grid-small margin-content padding-page">
		<div class="uk-width-large-1-4 uk-width-small-1 uk-width-medium-3-10 uk-text-center-small">
    		
            <dd class="level1"><a href="">Informasi </a></dd>
            <dd class="level2"><a href="#">Fasilitas</a></dd> 

    	</div>
		<div class="uk-width-large-1-2 uk-width-small-1 uk-width-medium-7-10 uk-text-center">
			<h1> Facility </h1>
    	</div>
    	<div class="uk-width-large-1-4 uk-width-small-1 uk-width-medium-7-10 uk-text-left">
			&nbsp;
    	</div>
	</div>

	<div class="uk-grid margin-content padding-page">
		
		<div class="uk-width-large-1-2">
			<div class="uk-thumbnail info uk-thumbnail-expand">                 
    			<img src="images/img.jpg">
    		</div>
    	</div>
    	<div class="uk-width-large-1-2 content">
    		<ul class="paragraph-bullet">
                <li> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod </li>
                <li> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod </li>
                <li> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod </li>
                <li> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod </li>
                <li> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod </li>      
            </ul>
    	</div>

    	<div class="uk-width-large-1-2 content">
    		<ul class="paragraph-bullet">
                <li> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod </li>
                <li> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod </li>
                <li> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod </li>
                <li> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod </li>
                <li> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod </li>      
            </ul>
    	</div>
    	<div class="uk-width-large-1-2">
    		<div class="uk-thumbnail info uk-thumbnail-expand">                 
    			<img src="images/img.jpg">
    		</div>
    	</div>

	</div>


	<!-- LINE -->
	
	<div class="uk-grid margin-content">
		<div class="uk-width-large-1">
    		<hr class="line">
    	</div>
	</div>

	<!-- Footer -->
	
	<?php include('footer.php'); ?>

</body>
</html>