<?php
//session_start(); 

//include("captcha/captcha.php"); 

?>
<!DOCTYPE html>
<html>
    <head>
    	<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="A layout example that shows off a responsive photo gallery.">
        <title></title>
        <link rel="stylesheet" href="css/uikit.min.css" />
        <link rel="stylesheet" href="css/base.css" />

        <!--- CSS Componen -->
        <link rel="stylesheet" href="css/components/slideshow.css" />

        <script src="js/jquery.js"></script>
        <script src="js/uikit.min.js"></script>


        <!-- JS componen -->
        <script src="js/components/slideshow.js"></script> 

        <!-- Slider Cover Flow -->
        <link rel="stylesheet" href="slider/css/demo.css---">
   		<link rel="stylesheet" href="slider/src/css/jquery.flipster.css">
    	<link rel="stylesheet" href="slider/css/flipsternavtabs.css--">


    	<script type="text/javascript">
    	 //$(function(){ $(".uk-slideshow").slideshow({ height: '200px' }); }); ....
    	</script>
    </head>
    <body>

<div class="uk-container uk-container-center">
	<!--  MENU -->
    
    <div class="uk-grid base">
    	<div class="uk-width-1">
    		<div class="">
    			<div>
    				
    				<?php include('menu.php'); ?>
    			</div>
    		</div>
    	</div>
	</div>

	<!-- SLIDER -->

	<div class="uk-grid margin-slider">
    	<div class="uk-width-1">
    		<div class="">
    			<ul class="uk-slideshow" data-uk-slideshow="{autoplay:true}">
    				<li><img src="images/Ceiling.png"></li>
    				<li><img src="images/Doorway.png"></li>
    				<li><img src="images/Frontdoor.png"></li>
    			</ul>
    		</div>
    	</div>
	</div>

	<!-- CONTENT -->

	<div class="uk-grid uk-grid-small margin-content padding-page-contact">
		<div class="uk-width-large-1-4 uk-width-small-1 uk-width-medium-3-10 uk-text-center-small">
    	   
            <dd class="level1"><a href="">Informasi </a></dd>
            <dd class="level2"><a href="#">Hubungi</a></dd> 

    	</div>
		<div class="uk-width-large-1-2 uk-width-small-1 uk-width-medium-7-10 uk-text-center">
			<h1> Hubungi </h1>
    	</div>
    	<div class="uk-width-large-1-4 uk-width-small-1 uk-width-medium-7-10 uk-text-left">
			&nbsp;
    	</div>
	</div>

	
    


    <div class="uk-grid uk-grid-small margin-content padding-page-contact">
		
        
        <div class="uk-width-large-4-10 uk-width-medium-4-10 uk-width-small-1">
            

            <div class="uk-grid uk-grid-small contact-phone">
                <div class="uk-width-medium-1-4 uk-width-small-1-4"> <img src="images/maps.png"> </div>
                <div class="uk-width-medium-2-4 uk-width-small-2-4">
                    <span class="font-small">
                        Jl. Raya Taman Mini <br>
                        Jakarta Timur 13560 <br>
                        Indonesia
                    </span> 
                </div>
            </div>
            <div class="uk-grid uk-grid-small contact-phone">
                <div class="uk-width-medium-1-4 uk-width-small-1-4"> <img src="images/phone.png"> </div>
                <div class="uk-width-medium-2-4 uk-width-small-2-4">
                     <span class="font-small">
                        Jl. Raya Taman Mini <br>
                        Jakarta Timur 13560 <br>
                        Indonesia
                    </span> 
                </div>
            </div> 
            <div class="uk-grid uk-grid-small contact-phone">
                <div class="uk-width-medium-1-4 uk-width-small-1-4"> <img src="images/email.png"> </div>
                <div class="uk-width-medium-2-4 uk-width-small-2-4">
                     <span class="font-small">
                        Jl. Raya Taman Mini <br>
                        Jakarta Timur 13560 <br>
                        Indonesia
                    </span> 
                </div>
            </div>
 
                
            <div class="margin-content">

               
                <form class="uk-form uk-form-horizontal sasana-form" method="POST" action="#">

                                <!--<div class="uk-form-row">
                                    <span id="results"></span> 
                                </div>-->

                                <div class="uk-form-row">
                                    <label class="uk-form-label" for="form-h-it">Name:</label>
                                    <div class="uk-form-controls name">
                                        <input type="text" placeholder="" name="name" id="name">
                                    </div>
                                </div>
                                <div class="uk-form-row">
                                    <label class="uk-form-label" for="form-h-ip">Email:</label>
                                    <div class="uk-form-controls email">
                                        <input type="text" placeholder="" name="email" id="email">
                                    </div>
                                </div>
                                <div class="uk-form-row">
                                    <label class="uk-form-label" for="form-h-ip">Handphone:</label>
                                    <div class="uk-form-controls handphone">
                                        <input type="text" placeholder="" name="handphone" id="handphone">
                                    </div>
                                </div> 
                                <div class="uk-form-row">
                                    <label class="uk-form-label" for="form-h-t">Pesanan:</label>
                                    <div class="uk-form-controls">
                                        <textarea cols="30" rows="5" placeholder="Textarea text" name="pesanan" id="pesanan"></textarea>
                                    </div>
                                </div>
                                <div class="uk-form-row">
                                    <label class="uk-form-label">Capcha:</label>
                                    <div class="uk-form-controls">
                                        
                                        <!--<div class="uk-grid uk-grid-small">
                                            <div class="uk-width-medium-4-10">
                                                <img src="images/capcha.jpg" width="120">
                                                <?php echo '<img src="captcha/captcha-bak.php" id="captcha" alt="CAPTCHA code" width="120">'; ?>
                                                <small><a href="#captcha" id="reload">refresh</a></small></p>   <span class="error-captcha"></span>
                                            </div>
                                            <div class="uk-width-medium-4-10 uk-text-right" style="padding-top:12px;"><input type="text" name="captcha" id="captcha" placeholder="" value=""> </div>
                                            <div class="uk-width-medium-1-1 uk-text-right"><button class="uk-button icon-button abs-button-contact" name="submit"></button></div>
                                        </div> -->
 

                                        <div class="uk-grid uk-grid-medium">
                                            <div class="uk-width-medium-8-10"> 
                                                <div class="uk-grid uk-grid-small">
                                                    <div class="uk-width-medium-1-2 uk-width-small-1-1">
                                                        <?php echo '<img src="captcha/captcha-bak.php" id="captcha" alt="CAPTCHA code" width="120">'; ?>
                                                         <small><a href="#captcha" id="reload">refresh</a></small></p>   <span class="error-captcha"></span>
                                                    </div>
                                                    <div class="uk-width-medium-1-2 uk-width-small-1-1 uk-text-right uk-text-left-small">
                                                        <div class="uk-vertical-align uk-panel" style="height: 47px;">
                                                            <div class="uk-vertical-align-middle">
                                                                <input type="text" name="captcha" id="captcha" placeholder="" class="uk-form-small">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> 
                                            </div>
                                            <div class="uk-width-medium-2-10 uk-text-right uk-text-left-small">
                                                <div class="uk-vertical-align uk-panel" style="height: 55px;">
                                                    <div class="uk-vertical-align-middle">
                                                        <button class="uk-button icon-button" name="submit"></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>  


                </form> 
 

            </div> 

        </div>

        <div class="uk-width-large-6-10 uk-width-medium-4-10 uk-width-small-1 uk-text-right uk-text-center-small">
             <img src="images/location.jpg" class="img-res">
        </div>
        <div class="popup-success-box" id="popup-success-box">
            <div class="popup-success">
                <div class="font-medium">Pertanyaan anda telah kami terima</div>
                <a href="" class="ok-button"></a>
            </div>
        </div>
         

	</div>


	<!-- LINE -->
	
	<div class="uk-grid margin-content">
		<div class="uk-width-large-1">
    		<hr class="line">
    	</div>
	</div>

        
	<!-- Footer -->
	
	<?php include('footer.php'); ?>

</body>
</html>