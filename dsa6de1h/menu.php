<?php $uri = basename($_SERVER['PHP_SELF']);?>
<nav class="uk-navbar">
					    <ul class="uk-navbar-nav uk-hidden-small uk-navbar-nav-custome">
					        <li class="<?php if( $uri == 'index.php') echo 'uk-active'; ?>">
					        	<a href="index.php">  
		                        	<img src="images/icon/home.png" border="0" alt=""> 
		                        	<span>Home</span>
		                        </a>
					        </li>
					        <li class="uk-parent <?php if( $uri == 'info.php') echo 'uk-active'; ?>" data-uk-dropdown="" aria-haspopup="true" aria-expanded="false">
					        	<a href="info.php">  
		                        	<img src="images/icon/informasi.png" border="0" alt=""> 
		                        	
		                        	<span>Informasi</span>

		                        	<div class="uk-dropdown uk-dropdown-navbar">
                                       <ul class="uk-nav uk-nav-navbar sub-menu"> 
                                          <li><a href="fasility.php"><span>Facility</span></a></li> 
                                          <li><a href="hubungi.php"><span>Hubungi</span></a></li>
                                       </ul>
                                    </div>
		                        </a>
		                        
					        </li>
					        <li class="<?php if( $uri == 'daftar.php') echo 'uk-active'; ?>">
					        	<a href="daftar.php">  
		                        	<img src="images/icon/harga.png" border="0" alt=""> 
		                        	<span>
		                        	Daftar Harga   </span>
		                        </a>
					        </li>
					        <li class="<?php if( $uri == 'rekanan.php') echo 'uk-active'; ?>">
					        	<a href="rekanan.php">  
		                        	<img src="images/icon/daftar.png" border="0" alt="">  
		                        	<span>
		                        	Daftar Rekanan</span>   
		                        </a>
					        </li>
					        <li class="<?php if( $uri == 'gallery.php') echo 'uk-active'; ?>">
					        	<a href="gallery.php">  
		                        	<img src="images/icon/gallery.png" border="0" alt="">
		                        	<span>
		                        	Gallery </span>
		                        </a>
					        </li>
					        <li class="<?php if( $uri == 'balroom.php') echo 'uk-active'; ?>">
					        	<a href="balroom.php">  
		                        	<img src="images/icon/ballroom.png" border="0" alt=""> 
		                        	<span>
		                         	Ketersediaan Ballroom  </span>
		                        </a>
					        </li>
					        <li class="<?php if( $uri == 'tari.php') echo 'uk-active'; ?>">
					        	<a href="tari.php">  
		                        	<img src="images/icon/tari.png" border="0" alt=""> 
		                        	<span>
		                        	Tari Tarian Adat </span>
		                        </a>
					        </li>
					    </ul>
					</nav> 
					
					<nav class="uk-navbar"> 
				    <a href="#my-id" class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas="{target:'#my-id'}"></a>
					</nav>

					<div id="my-id" class="uk-offcanvas">
					    <div class="uk-offcanvas-bar">
					        <div class="uk-panel">
	                           <ul class="uk-nav uk-nav-side uk-nav-parent-icon">
						       	<li class="<?php if( $uri == 'index.php') echo 'uk-active'; ?>">
						        	<a href="index.php">  
			                        	<img src="images/icon/home.png" border="0" alt=""> 
			                        	<span>Home</span>
			                        </a>
						        </li>
						        <li>
						        	<a href="info.php">  
			                        	<img src="images/icon/informasi.png" border="0" alt=""> 
			                        	
			                        	<span>Informasi</span>
	                                       <ul class="uk-parent uk-open"> 
	                                          <li><a href="fasility.php"><span>Facility</span></a></li> 
	                                          <li><a href="hubungi.php"><span>Hubungi</span></a></li>
	                                     </ul> 
			                        </a>
			                        
						        </li>
						        <li class="<?php if( $uri == 'daftar.php') echo 'uk-active'; ?>">
						        	<a href="daftar.php">  
			                        	<img src="images/icon/harga.png" border="0" alt=""> 
			                        	<span>
			                        	Daftar Harga   </span>
			                        </a>
						        </li>
						        <li class="<?php if( $uri == 'rekanan.php') echo 'uk-active'; ?>">
						        	<a href="rekanan.php">  
			                        	<img src="images/icon/daftar.png" border="0" alt="">  
			                        	<span>
			                        	Daftar Rekanan</span>   
			                        </a>
						        </li>
						        <li class="<?php if( $uri == 'gallery.php') echo 'uk-active'; ?>">
						        	<a href="gallery.php">  
			                        	<img src="images/icon/gallery.png" border="0" alt="">
			                        	<span>
			                        	Gallery </span>
			                        </a>
						        </li>
						        <li class="<?php if( $uri == 'balroom.php') echo 'uk-active'; ?>">
						        	<a href="balroom.php">  
			                        	<img src="images/icon/ballroom.png" border="0" alt=""> 
			                        	<span>
			                         	Ketersediaan Ballroom  </span>
			                        </a>
						        </li>
						        <li class="<?php if( $uri == 'tari.php') echo 'uk-active'; ?>">
						        	<a href="tari.php">  
			                        	<img src="images/icon/tari.png" border="0" alt=""> 
			                        	<span>
			                        	Tari Tarian Adat </span>
			                        </a>
						        </li>
						    	</ul>
					        </div>
					    </div>
					</div> 
