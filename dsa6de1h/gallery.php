<!DOCTYPE html>
<html>
    <head>
    	<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="A layout example that shows off a responsive photo gallery.">
        <title></title>
        <link rel="stylesheet" href="css/uikit.min.css" />
        <link rel="stylesheet" href="css/base.css" />

        <!--- CSS Componen -->
        <link rel="stylesheet" href="css/components/slideshow.css" />

        <script src="js/jquery.js"></script>
        <script src="js/uikit.min.js"></script>


        <!-- JS componen -->
        <script src="js/components/slideshow.js"></script> 

        <!-- Slider Cover Flow -->
        <link rel="stylesheet" href="slider/css/demo.css---">
   		<link rel="stylesheet" href="slider/src/css/jquery.flipster.css">
    	<link rel="stylesheet" href="slider/css/flipsternavtabs.css--">


    	<script type="text/javascript">
    	 //$(function(){ $(".uk-slideshow").slideshow({ height: '200px' }); });
    	</script>
    </head>
    <body>

<div class="uk-container uk-container-center">
	<!--  MENU -->
    
    <div class="uk-grid">
    	<div class="uk-width-1">
    		<div class="">
    			
    			<div>
    				
    				<?php include('menu.php'); ?>

    			</div>

    		</div>
    	</div>
	</div>

	<!-- SLIDER -->

	<div class="uk-grid margin-slider">
    	<div class="uk-width-1">
    		<div class="">
    			<ul class="uk-slideshow" data-uk-slideshow="{autoplay:true}">
    				<li><img src="images/Ceiling.png"></li>
    				<li><img src="images/Doorway.png"></li>
    				<li><img src="images/Frontdoor.png"></li>
    			</ul>
    		</div>
    	</div>
	</div>

	<!-- CONTENT -->

	<div class="uk-grid uk-grid-small margin-content padding-page">
		<div class="uk-width-large-1-4 uk-width-small-1 uk-width-medium-3-10 uk-text-center-small">
    	
            <dd class="level1"><a href="">Gallery </a></dd>
            <dd class="level2"><a href="#">Pernikahah</a></dd>
            <dd class="level3"><a href="#">Tradisional</a></dd> 

    	</div>
		<div class="uk-width-large-1-2 uk-width-small-1 uk-width-medium-7-10 uk-text-center">
			<h1> Pernikahan Traditional </h1>
    	</div>

    	<div class="uk-width-large-1-4 uk-width-small-1 uk-width-medium-7-10 uk-text-left">
			&nbsp;
    	</div>
	</div>



    <!-- 2 col --> 
	<div class="uk-grid margin-content padding-page"> 
        <div class="uk-width-large-1-1"> <h3>Lorem ipsum dolor sit amet</h3> </div> 
	</div>

    <div class="uk-grid uk-grid-medium-20 padding-page-img">
        <div class="uk-width-large-1-2">
            <div class="uk-thumbnail info uk-thumbnail-expand">                 
                <img src="images/img2.jpg">
            </div> 
        </div> 
        <div class="uk-width-large-1-2">
            <div class="uk-thumbnail info uk-thumbnail-expand">                 
                <img src="images/img2.jpg">
            </div> 
        </div>
    </div>
    <!-- / 2 col -->


    <!-- 3 col -->  
    <div class="uk-grid uk-grid-medium-20 margin-content-gallery padding-page">
        <div class="uk-width-large-1-3">
            <div class="uk-thumbnail info uk-thumbnail-expand">                 
                <img src="images/img3.jpg">
            </div> 
        </div> 
        <div class="uk-width-large-1-3">
            <div class="uk-thumbnail info uk-thumbnail-expand">                 
                <img src="images/img3.jpg">
            </div> 
        </div>
        <div class="uk-width-large-1-3">
            <div class="uk-thumbnail info uk-thumbnail-expand">                 
                <img src="images/img3.jpg">
            </div> 
        </div>
    </div>
     <!-- / 3 col -->

     <!-- 2 col --> 
    <div class="uk-grid margin-content padding-page"> 
        <div class="uk-width-large-1-1"> <h3>Lorem ipsum dolor sit amet</h3> </div> 
    </div>

    <div class="uk-grid uk-grid-medium-20 padding-page-img gallery-grid">
        <div class="uk-width-large-1-2">
            <div class="uk-thumbnail info uk-thumbnail-expand">                 
                <img src="images/img2.jpg">
            </div> 
        </div> 
        <div class="uk-width-large-1-2">
            <div class="uk-thumbnail info uk-thumbnail-expand">                 
                <img src="images/img2.jpg">
            </div> 
        </div>
        <div class="uk-width-large-1-2">
            <div class="uk-thumbnail info uk-thumbnail-expand">                 
                <img src="images/img2.jpg">
            </div> 
        </div> 
        <div class="uk-width-large-1-2">
            <div class="uk-thumbnail info uk-thumbnail-expand">                 
                <img src="images/img2.jpg">
            </div> 
        </div>
        <div class="uk-width-large-1-2">
            <div class="uk-thumbnail info uk-thumbnail-expand">                 
                <img src="images/img2.jpg">
            </div> 
        </div>
    </div>
    <!-- / 2 col -->

	<!-- LINE -->
	<div class="uk-grid margin-content">
		<div class="uk-width-large-1">
    		<hr class="line">
    	</div>
	</div>

	<!-- Footer -->
	
	<?php include('footer.php'); ?>

</body>
</html>