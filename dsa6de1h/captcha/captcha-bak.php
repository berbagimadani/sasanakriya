<?php
/* captcha.php file*/

	session_start();
	
	header("Expires: Tue, 01 Jan 2013 00:00:00 GMT"); 
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); 
	header("Cache-Control: no-store, no-cache, must-revalidate"); 
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	
	$chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';

    for ($i = 0; $i < 5; $i++) 
    {
        $randomString .= $chars[rand(0, strlen($chars)-1)];
    }
	
	$_SESSION['captcha'] = strtolower( $randomString );
	 $font_path = dirname(__FILE__) . '/fonts/';
	
	$bg_path = dirname(__FILE__) . '/backgrounds/';
	
	$im = @imagecreatefrompng(  $bg_path . '45-degree-fabric.png' ); 
	

	imagettftext($im, 30, 0, 10, 38, imagecolorallocate ($im, 0xFF, 0xFF, 0xFF), $font_path . 'times_new_yorker.ttf', $randomString);
	
	header ('Content-type: image/png');
	imagepng($im, NULL, 0);
	imagedestroy($im);

?>