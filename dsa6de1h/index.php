<!DOCTYPE html>
<html>
    <head>
    	<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="A layout example that shows off a responsive photo gallery.">
        <title></title>
        <link rel="stylesheet" href="css/uikit.min.css" />

        <link rel="stylesheet" href="css/base.css" />

        <script src="js/jquery.js"></script>
        <script src="js/uikit.min.js"></script>

        <link rel="stylesheet" href="slider/css/demo.css---">
   		<link rel="stylesheet" href="slider/src/css/jquery.flipster.css">
    	<link rel="stylesheet" href="slider/css/flipsternavtabs.css--">



    </head>
    <body>

<div class="uk-container uk-container-center base-container">
	<!--  MENU -->
    
    <div class="uk-grid">
    	<div class="uk-width-1">
    		<div class="">
    			 
    			<?php include('menu.php'); ?> 

    		</div>
    	</div>
	</div>

	<!-- SLIDER -->

	<div class="uk-grid margin-slider">
    	<div class="uk-width-1">
    		<div class="uk-panel- uk-panel-box-">
    		
    		<!-- Flipster List -->	
					<div class="flipster">
					  <ul>
					  	<li>
					  		<a href="#"> 
					  			<img src="images/slider/C-Ballroom.png"> 
					  		</a>
					  	</li>
				  		<li>
					  		<a href="#"> 
					  			<img src="images/slider/C-Lobby.png"> 
					  		</a>
					  	</li>
				  		<li>
					  		<a href="#"> 
					  			<img src="images/slider/C-Pintuutama.png">
					  		</a>
					  	</li>
				  		<li>
					  		<a href="#"> 
					  			<img src="images/slider/C-Plafon.png">
					  		</a>
					  	</li>
					  	<li>
					  		<a href="#"> 
					  			<img src="images/slider/C-tampakdepan.png">
					  		</a>
					  	</li>
				  		<li>
					  		<a href="#"> 
					  			<img src="images/slider/C-tampaksamping.png">
					  		</a>
					  	</li>
					  	<li>
					  		<a href="#"> 
					  			<img src="images/slider/C-tampakdepan.png">
					  		</a>
					  	</li>
				  		<li>
					  		<a href="#"> 
					  			<img src="images/slider/C-tampaksamping.png">
					  		</a>
					  	</li>
					  	<li>
					  		<a href="#"> 
					  			<img src="images/slider/C-tampakdepan.png">
					  		</a>
					  	</li>
				  		<li>
					  		<a href="#"> 
					  			<img src="images/slider/C-tampaksamping.png">
					  		</a>
					  	</li>
					  </ul>
					</div>
				<!-- End Flipster List -->

    		</div>
    	</div>
	</div>

	<!-- CONTENT -->

	<div class="uk-grid margin-content">
		<div class="uk-width-large-3-10 uk-width-small-1 uk-width-medium-3-10 uk-text-center-small">
    		<img src="images/logo.jpg" class="uk-responsive-height">
    	</div>
		<div class="uk-width-large-7-10 uk-width-small-1 uk-width-medium-7-10">
			<div class="uk-vertical-align" style="height: 200px;">
			    <div class="uk-vertical-align-middle">
			    	<p>
   				Sasana Kriya dengan gaya minimalis modern merupakan salah satu pilihan tempat terbaik untuk pernikahan, peluncuran produk, pameran, konferensi, konvensi, wisuda atau pertemuan lainnya. Dengan banyak pilihan rekanan rekanan katering, dekorasi dan ruang parkir yang luas, Sasana Kriya adalah pilihan utama untuk acara Anda
   					</p>
   					<div class="uk-text-right">
   						<a href="#" class="btn-readmore"></a>
   					</div>
			    </div>
			</div>
    	</div>
	</div>


	<!-- LINE -->
	
	<div class="uk-grid margin-content">
		<div class="uk-width-large-1">
    		<hr class="line">
    	</div>
	</div>

	<!-- Footer -->
	
	<?php include('footer.php'); ?>


</body>
</html>